/**
 * Created by Ticco on 8/4/2015.
 */

var Filters = function () {
    this.department = {
        filtered: false,
        name: 'department',
        value: ''
    };
    this.salary = {
        filtered: false,
        nameMin: 'salary_min',
        valueMin: 0,
        nameMax: 'salary_max',
        valueMax: 0
    };
    this.demand = {
        filtered: false,
        nameMin: 'demand_min',
        valueMin: 0,
        nameMax: 'demand_max',
        valueMax: 0
    };
    this.education = {
        filtered: false,
        name: 'education',
        value: ''
    };
    this.sort = {
        filtered: false,
        name: 'sort',
        value: '',
        nameDirection: 'order',
        valueDirection: ''

    };
    this.search = {
        filtered: false,
        name: 'search',
        value: ''
    }
};

var ProfessionsList = function () {

    this.filterAllowed = true;

    this.filters = new Filters;

    this.list = $('#professionsList');

    this.generateFilterLink = function () {
        var filters = {};

        if (this.filters.department.filtered) {
            filters[this.filters.department.name] = this.filters.department.value;
        }
        if (this.filters.salary.filtered) {
            filters[this.filters.salary.nameMin] = this.filters.salary.valueMin;
            filters[this.filters.salary.nameMax] = this.filters.salary.valueMax;
        }
        if (this.filters.demand.filtered) {
            filters[this.filters.demand.nameMin] = this.filters.demand.valueMin;
            filters[this.filters.demand.nameMax] = this.filters.demand.valueMax;
        }
        if (this.filters.education.filtered) {
            filters[this.filters.education.name] = this.filters.education.value;
        }
        if (this.filters.sort.filtered) {
            filters[this.filters.sort.name] = this.filters.sort.value;
            filters[this.filters.sort.nameDirection] = this.filters.sort.valueDirection;
        }
        // if search needed, will be updated later //

        var urlString = Object.keys(filters).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(filters[key]);
        }).join('&');
        return {getParams: urlString, postParams: filters};
    };
    this.removeSortedClasses = function () {
        $('#sortDepartment').removeClass('arrow-up').removeClass('arrow-down');
        $('#sortSalary').removeClass('arrow-up').removeClass('arrow-down');
        $('#sortDemand').removeClass('arrow-up').removeClass('arrow-down');
        $('#sortEducation').removeClass('arrow-up').removeClass('arrow-down');
    };
    this.prepareForSorting = function (criteria, sortIconID) {
        this.filters.sort.filtered = true;
        if (this.filters.sort.value == criteria) {
            if (this.filters.sort.valueDirection == 'desc') {
                this.filters.sort.valueDirection = 'asc';
                $('#' + sortIconID).removeClass('arrow-up').addClass('arrow-down');
            } else {
                this.filters.sort.valueDirection = 'desc';
                $('#' + sortIconID).addClass('arrow-up').removeClass('arrow-down');
            }
        } else {
            this.filters.sort.value = criteria;
            this.filters.sort.valueDirection = 'asc';
            $('#' + sortIconID).removeClass('arrow-up').addClass('arrow-down');
        }
        return this;
    };
    this.getList = function () {
        if (!this.filterAllowed) return;

        var link = this.generateFilterLink();

        console.log(link);
        //ToDo: Stex ajax a gnum
        // link.postParams -@ ajax-i data-um dnelov (inq@ object a, karas miangamic dnes data: postParams,)
        // veradardzvox informacian petq a lcnel $('#professionsList')-i mej, iran naxoroq datarkelov - $('#professionsList').empty();
        // eti karam es lcnem HTML-@ generacnelov JS-um, ete duq veradardznum eq json-ov kargavorvac string,
        // isk ete veradardznum eq HTML arden patrasti - et depqum prosto veradardzrac HTML-@ piti append arvi empty-ic heto - $('#professionsList').append(HTML)

    };


};

$(document).ready(function () {

    var PL = new ProfessionsList();

    $('#filterDepartment').selectize({
        onInitialize: function () {
            $('#filterDepartment').parent().find('input').attr('readonly', '');
        }
    }).on('change', function () {
        var department = $(this).val();
        if (department != 0) {
            PL.filters.department.filtered = true;
            PL.filters.department.value = department;
            $('#filteredDepartment').addClass('active').find('.filter-text').text($('#filterDepartment option[selected]').text());
        } else {
            PL.filters.department.filtered = false;
            PL.filters.department.value = null;
            $('#filteredDepartment').removeClass('active').find('.filter-text').text();
        }
        PL.getList();
    });

    $('#filterEducation').selectize({
        onInitialize: function () {
            $('#filterEducation').parent().find('input').attr('readonly', '');
        }
    }).on('change', function () {
        var education = $(this).val();
        if (education != 0) {
            PL.filters.education.filtered = true;
            PL.filters.education.value = education;
            $('#filteredEducation').addClass('active').find('.filter-text').text($('#filterEducation option[selected]').text() + ' образование');
        } else {
            PL.filters.education.filtered = false;
            PL.filters.education.value = null;
            $('#filteredEducation').removeClass('active').find('.filter-text').text();
        }
        PL.getList();
    });

    $('#filterSort').selectize({
        onInitialize: function () {
            $('#filterSort').parent().find('input').attr('readonly', '');
        }
    }).on('change', function () {
        var sort = $(this).val();
        switch (sort) {
            case '1':
                $('#sortDepartment').trigger('click');
                break;
            case '2':
                $('#sortSalary').trigger('click');
                break;
            case '3':
                $('#sortDemand').trigger('click');
                break;
            case '4':
                $('#sortEducation').trigger('click');
                break;
            default:
        }
    });

    var salaryFilter = $("#salaryFilter");
    salaryFilter
        .slider({
            range: true,
            min: salaryFilter.data('minValue'),
            max: salaryFilter.data('maxValue'),
            step: 1000,
            values: [salaryFilter.data('start'), salaryFilter.data('end')],
            change: function (event, ui) {
                var vals = salaryFilter.slider('values');
                salaryFilter.parent().find('.filter-values-wrapper').find('.filter-min-value').text(Math.round(vals[0] / 1000) + ' тыс.').end()
                    .find('.filter-max-value').text(Math.round(vals[1] / 1000) + ' тыс.').end();
                if (salaryFilter.slider('option', 'min') != vals[0] || salaryFilter.slider('option', 'max') != vals[1]) {
                    PL.filters.salary.filtered = true;
                    PL.filters.salary.valueMin = vals[0];
                    PL.filters.salary.valueMax = vals[1];
                    $('#filteredSalary').addClass('active').find('.filter-text').text(Math.round(vals[0] / 1000) + ' - ' + Math.round(vals[1] / 1000) + ' тыс. руб.');
                } else {
                    PL.filters.salary.filtered = false;
                    PL.filters.salary.valueMin = vals[0];
                    PL.filters.salary.valueMax = vals[1];
                    $('#filteredSalary').removeClass('active').find('.filter-text').text();
                }
                PL.getList();
            }
        })
        .parent()
        .find('.filter-values-wrapper').find('.filter-min-value').text(Math.round(salaryFilter.data('start') / 1000) + ' тыс.').end()
        .find('.filter-max-value').text(Math.round(salaryFilter.data('end') / 1000) + ' тыс.').end()
    ;

    var demandFilter = $("#demandFilter");
    demandFilter
        .slider({
            range: true,
            min: 0,
            max: 3,
            step: 1,
            values: [0, 3],
            change: function (event, ui) {
                var vals = demandFilter.slider('values');
                if (vals[0] == vals[1]) {
                    if (vals[0] == 0) {
                        demandFilter.slider('values', 1, vals[0] + 1);
                    } else {
                        demandFilter.slider('values', 0, vals[1] - 1);
                    }
                    return;
                }
                var demandText = '';
                switch (demandFilter.slider('values', 1)) {
                    case 1:
                        demandFilter.addClass('slider-magenta').removeClass('slider-blue').removeClass('slider-green');
                        demandText = 'Низкая';
                        break;
                    case 2:
                        demandFilter.removeClass('slider-magenta').addClass('slider-blue').removeClass('slider-green');
                        demandText = 'Средняя';
                        break;
                    case 3:
                        demandFilter.removeClass('slider-magenta').removeClass('slider-blue').addClass('slider-green');
                        demandText = 'Низкая';
                        break;
                    default:
                        demandFilter.addClass('slider-magenta').removeClass('slider-blue').removeClass('slider-green');
                        demandText = 'Низкая';
                }

                if (demandFilter.slider('option', 'min') != vals[0] || demandFilter.slider('option', 'max') != vals[1]) {
                    PL.filters.demand.filtered = true;
                    PL.filters.demand.valueMin = vals[0];
                    PL.filters.demand.valueMax = vals[1];
                    $('#filteredDemand').addClass('active').find('.filter-text').text(demandText + ' востребованность');
                } else {
                    PL.filters.demand.filtered = false;
                    PL.filters.demand.valueMin = vals[0];
                    PL.filters.demand.valueMax = vals[1];
                    $('#filteredDemand').removeClass('active').find('.filter-text').text();
                }
                PL.getList();
            }
        });

    $('.remove-filter').click(function (e) {
        var filterTag = $(this).parents('.filter-tag');
        switch (filterTag.data('filter')) {
            case 'department':
                $('#filterDepartment').parent().find('.selectize-dropdown-content').find('[data-value="0"]').click().trigger('click');
                break;
            case 'salary':
                salaryFilter.slider('values', [salaryFilter.data('start'), salaryFilter.data('end')]);
                break;
            case 'demand':
                demandFilter.slider('values', [0, 3]);
                break;
            case 'education':
                $('#filterEducation').parent().find('.selectize-dropdown-content').find('[data-value="0"]').click().trigger('click');
                break;
        }
    });

    $('#filterModal').on('shown.bs.modal', function () {
        PL.filterAllowed = false;
        setTimeout(function () {
            salaryFilter.slider("values", 0, salaryFilter.slider('values', 0) + 1);
            PL.filterAllowed = true;
        }, 100);
    });

    $('#sortDepartment').click(function () {
        PL.removeSortedClasses();
        PL.list.fadeOut(500, function () {
            PL.prepareForSorting('department', 'sortDepartment').getList();
            PL.list.fadeIn(500);
        });
    });

    $('#sortSalary').click(function () {
        PL.removeSortedClasses();
        PL.list.fadeOut(500, function () {
            PL.prepareForSorting('salary', 'sortSalary').getList();
            PL.list.fadeIn(500);
        });
    });

    $('#sortDemand').click(function () {
        PL.removeSortedClasses();
        PL.list.fadeOut(500, function () {
            PL.prepareForSorting('demand', 'sortDemand').getList();
            PL.list.fadeIn(500);
        });
    });

    $('#sortEducation').click(function () {
        PL.removeSortedClasses();
        PL.list.fadeOut(500, function () {
            PL.prepareForSorting('education', 'sortEducation').getList();
            PL.list.fadeIn(500);
        });
    });

});