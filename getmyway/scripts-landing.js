/**
 * Created by Ticco on 7/31/2015.
 */

$(document).ready(function () {

    var topSlider = $('#topSlider').bxSlider();
    var topProfessions = $('#topProfessions').bxSlider();

    $(window).on("resize", function () {
        topSlider.reloadSlider();
        topProfessions.reloadSlider();
    });

    $('#loginForm').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {

                    },
                    stringLength: {
                        enabled: true,
                        min: 3
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        alert('Send login form now! (by ajax) :) ');
    });

    $('#registerForm').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        alert('Send register form now! (by ajax) :) ');
    });

    $('#forgotPasswordForm').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        alert('Send register form now! (by ajax) :) ');
    });

    $('#provideEmailForm').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        alert('Send email provideing form now! (by ajax) :) ');
    });

    $('#welcomeBackForm').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {

                    },
                    stringLength: {
                        enabled: true,
                        min: 3
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        alert('Send welcome back form now! (by ajax) :) ');
    });

    $('#resetPasswordForm').bootstrapValidator({
        fields: {
            password1: {
                validators: {
                    notEmpty: {

                    },
                    stringLength: {
                        enabled: true,
                        min: 3
                    }
                }
            },
            password2: {
                validators: {
                    notEmpty: {

                    },
                    stringLength: {
                        enabled: true,
                        min: 3
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        e.preventDefault();
        alert('Send reset password form now! (by ajax) :) ');
    });

});