/**
 * Created by Ticco on 7/15/2015.
 */

function CloseSlide() {
    $('body').removeClass('slide-active');
    $('.slide-menu').modal('hide');
}
function OpenSlide() {
    $('body').addClass('slide-active');
}

$(document).ready(function () {

    var ctrlDown = false;
    var ctrlKey = 17, vKey = 86, cKey = 67;

    $(document).keydown(function(e) {
        if (e.keyCode == ctrlKey) ctrlDown = true;
    }).keyup(function(e) {
        if (e.keyCode == ctrlKey) ctrlDown = false;
    });

    $(".no-copy-paste").keydown(function(e) {
        if (ctrlDown && (e.keyCode == vKey || e.keyCode == cKey)) return false;
    }).on('dragstart', function(e){
        e.preventDefault();
        return false;
    }).on('dragstart selectstart', function(e){
        e.preventDefault();
        return false;
    });

    //$('#page-content').width($('body').width());

    $('#slide-nav.navbar .container').append($('<div id="navbar-height-col"></div>'));

    $('.navbar-toggle').click(function(){
        if ($('body').hasClass('slide-active')) {
            CloseSlide();
        } else {
            OpenSlide();
        }
    });

    $('#page-content').click(function(){
        $('body').removeClass('slide-active');
    });

    $('#sortingSelector li').click(function(e){
        e.preventDefault();
        if ($(this).hasClass('active')) return;
        $('#sortingSelector').find('li').removeClass('active');
        $(this).addClass('active');
    });

    $(window).on("resize", function () {
        $('#page-content').width($('body').width());
    });

    $('#searchForm')
        .find('.btn-search').on('click', function(){
            if ($(this).parents('form').hasClass('active')) {
                $(this).parents('form').submit();
            } else {
                $('#searchForm').addClass('active');
            }
        }).end()
        .find('.btn-cancel-search').on('click', function(){
            $('#searchForm').removeClass('active').find('input').val('');
        }).end()
        .find('input').on('keyup paste', function() {
            if (!$(this).parents('form').hasClass('active')) {
                $('#searchForm').addClass('active');
            }
        }).end();

    $('.form-group')
        .find('input').on('keyup paste', function(){
            if ($(this).attr('readonly')) return;
            var clearingIcon = $(this).parents('.form-group').find('.clear-input');
            if ($(this).val() != '') {
                clearingIcon.addClass('visible');
            } else {
                clearingIcon.removeClass('visible');
            }
        }).end()
        .find('.clear-input').on('click', function(){
            if ($(this).hasAttribute('readoly')) return;
            var input = $(this).parents('.form-group').find('input');
            $(this).removeClass('visible').parents('.form-group').find('input').val('');
            $(this).parents('form').bootstrapValidator('revalidateField', input);
        }).end();

    $('.slide-menu').on('shown.bs.modal', function () {
        OpenSlide();
    });

    var circleProgress = $('#testProgress');
    if (circleProgress.length > 0) {
        var strokeColor = circleProgress.data('color');
        var progressValue = parseInt(circleProgress.data('value')) / 100;
        var circle = new ProgressBar.Circle('#testProgress', {
            color: strokeColor,
            trailColor: '#cccccc',
            strokeWidth: 10,
            trailWidth: 10,
            duration: 1000,
            text: {
                value: '0%'
            },
            step: function (state, bar) {
                bar.setText((bar.value() * 100).toFixed(0) + '%');
            }
        });
        circle.animate(progressValue);
    }

    var circleProgressSliding = $('#testProgressSliding');
    if (circleProgressSliding.length > 0) {
        var strokeColorSliding = circleProgressSliding.data('color');
        var progressValueSliding = parseInt(circleProgressSliding.data('value')) / 100;
        var circleSliding = new ProgressBar.Circle('#testProgressSliding', {
            color: strokeColorSliding,
            trailColor: '#cccccc',
            strokeWidth: 10,
            trailWidth: 10,
            duration: 1000,
            text: {
                value: '0%'
            },
            step: function (state, bar) {
                bar.setText((bar.value() * 100).toFixed(0) + '%');
            }
        });
        circleSliding.animate(progressValueSliding);
    }

});