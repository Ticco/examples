
function Request() {
    this.requestId = null;
    this.requestOpened = null;
    this.userId = null;
    this.phone= null;
    this.userName = null;
    this.conversationId = null;
    this.callStartTime = null;
    this.callActive = false;
    this.conclusionExists = false;
    this.waitTime = 0;
    this.requestType = null;
    this.tableIndex = null;
    this.doctorId = null;
    this.doctorName = null;
    this.doctorProfession = null;
    this.unread = 1;
    this.markAsReadSending = false;
}

function OpenTok() {
    var _this=this;
    this.openTokApiKey = null;
    this.publishVideo = false;
    this.publishAudio = false;
    this.session = null;
    this.sessionId = null;
    this.token = null;
    this.publisher = null;
    this.deviceManager = null;
    this.subscriber = null;
    this.stream = null;
    this.requestType = null;
    this.pubOptions = null;
    this.streamVideo = null; // Whether to stream video in video call mode ("answer only with audio" case)
    this.devicesDisallowed = false;
    this.askingToAllow = false;
    this.browser = null;
    this.fullscreen = false;

    this.setPubOptions = function(pubOptions) {
        this.pubOptions = pubOptions;
    };

    this.toggleFullScreen = function() {
        if (CM.consultationWindow.hasClass('fullscreen')) {
            CM.consultationWindow.find('.action-buttons').find('.messages').removeClass('new');
            CM.consultationWindow.removeClass('fullscreen');
        } else {
            $('object').first().next().css('width','100%');
            $('object').first().next().css('height','100%');
            CM.consultationWindow.addClass('fullscreen');
        }
    };

    this.sessionConnectedHandler = function(event) {

        var pub_id;
        if (_this.pubOptions.publishVideo) {
            $("#videos").append($(_this.pubs.videopub));
            pub_id = "video_pub";
        } else {
            $("#videos").append($(_this.pubs.audiopub));
            pub_id = "audio_pub";
        }

        _this.pubOptions.publishVideo = _this.streamVideo;

        _this.publisher = TB.initPublisher(_this.openTokApiKey, pub_id, _this.pubOptions);
        _this.publisher.setStyle({buttonDisplayMode: "off"});
        _this.publisher.setStyle({showMicButton: false});
        _this.publisher.setStyle({showSettingsButton: false});
        _this.publisher.setStyle({showCameraToggleButton: false });
        id = '#' + _this.publisher.id;
        $(id).css('position','absolute');
        $(id).css('z-index',9999);
    //    _this.publisher.setStyle('position','absolute')

        $('#'+pub_id+" .OT_video-poster").css('background-image', 'url(/mobile/user/userpic/'+$('#myId').val()+'/thumb/)');

        _this.session.publish(_this.publisher);
        _this.subscribeToStreams(event.streams);

      //  _this.publisher.addEventListener("accessDialogOpened", _this.animateAllowStreamNotification);
//        _this.publisher.addEventListener("accessDialogClosed", _this.animateClosingAllowStreamNotification, false);
//        _this.publisher.addEventListener("accessAllowed", _this.animateClosingAllowStreamNotification, false);
//        _this.publisher.addEventListener("accessDenied", _this.animateDisabledDeviceNotification, false);
        _this.publisher.addEventListener('accessAllowed', function() {
            if(typeof _this.startRecording=='function') {
                _this.startRecording();
            }
            _this.animateClosingAllowStreamNotification();
        });
        _this.publisher.addEventListener('accessDenied', function() {
            _this.devicesDisallowed = true;
            setAchtungText('Блокирован доступ к камере и микрофону. <a href="/help/39/">Как включить?</a>');
            _this.animateDisabledDeviceNotification(true);
        });
        if(version==2) {
            _this.publisher.addEventListener('accessDialogOpened', function() {
                if (_this.devicesDisallowed) return;
                askingToAllow = true;
                setAchtungText('Требуется доступ к камере и микрофону');
                setTimeout(function(){
                    if (askingToAllow) {
                        _this.animateAllowStreamNotification();
                    }
                }, 3000);
            });
        }
        _this.publisher.addEventListener('accessDialogClosed', function() {
            _this.animateClosingAllowStreamNotification();
        });

        _this.session.addEventListener("streamDestroyed", _this.clientDisconnected, false);

/*
        _this.publisher.addEventListener({
            accessDialogOpened: function accessDialogOpenedHandler(event) {
                _this.animateAllowStreamNotification();
            },
            accessDialogClosed: function (event) {
                _this.animateClosingAllowStreamNotification();
            }
        });
        _this.publisher.addEventListener({
            accessAllowed: function (event) {
                // The user has granted access to the camera and mic.
            },
            accessDenied: function accessDeniedHandler(event) {
                _this.animateDisabledDeviceNotification();
            }
        });*/
    };

    this.subscribeToStreams = function(streams) {
        for (var i = 0; i < streams.length; i++) {
            stream = streams[i];
            this.requestType = 'video';
            if (stream.connection.connectionId != this.session.connection.connectionId) {
                if (this.requestType == 'video') {
                    if ($('#video_sub')) {
                        $("#video_sub").remove();
                    }
                    $("#videos").append(this.pubs.videosub);
                    var subscriber = this.session.subscribe(stream, "video_sub");
                    element = document.getElementById(subscriber.id);
                    console.log(subscriber.getAudioVolume())
                    subscriber.setAudioVolume(100);
                    console.log(subscriber.getAudioVolume())
                    id = '#' + subscriber.id;
                    $(id).css("width", 449);
                    $(id).css("height", 336);
                    $(id).css('position','absolute');
                } else if (this.requestType == 'audio') {
                    if ($('#audio_sub')) {
                        $("#audio_sub").remove();
                    }
                    $("#videos").append(this.pubs.audiosub);
                    var subscriber = this.session.subscribe(stream, "audio_sub");
                    element = document.getElementById(subscriber.id);
                    id = '#' + subscriber.id;
                    $(id).css("width", 40);
                    $(id).css("height", 30);
                }
            }
        }
    };

    this.streamCreatedHandler = function(event) {
        _this.subscribeToStreams(event.streams);
    };

    this.initOpentok = function() {
        window.onbeforeunload=CM.unload;
        this.session = TB.initSession(this.sessionId);

        TB.setLogLevel(TB.NONE);
        this.session.addEventListener("sessionConnected", this.sessionConnectedHandler);
        this.session.addEventListener("streamCreated", this.streamCreatedHandler);
        this.session.connect(this.openTokApiKey, this.token);

    };

    this.disconnectStuff = function() {

        //TODO veranayel!!!

        if (this.session) {
            if (this.publisher) {
                this.session.unpublish(this.publisher);
                this.publisher.destroy();
                this.publisher = null;
            }
            if (this.stream) {
                this.session.unsubscribe(this.stream);
                this.stream = null;
            }
            this.session.disconnect();
            this.session = null;
        }
    };

    this.toggleVideo = function() {
        this.publishVideo = !this.publishVideo;
        this.publisher.publishVideo(this.publishVideo);
        if ($('.action-buttons .video').css('background-position') == "0% 0%" || $('.action-buttons .video').css('background-position') == "0px -55px") {
            $('.action-buttons .video').css('background-position', "0px 0px");
        } else {
            $('.action-buttons .video').css('background-position', "0px -55px");
        }
    };

    this.toggleAudio = function() {
        if (this.publishAudio) {
            $('.mic').addClass('disabled');
            this.publishAudio = false;
        } else {
            $('.mic').removeClass('disabled');
            this.publishAudio = true;
        }
     //   this.publishAudio = !this.publishAudio;
        this.publisher.publishAudio(this.publishAudio);
    };

    this.clientDisconnected=function(e){
        if(e.reason=="networkDisconnected" || e.reason=="clientDisconnected" || e.reason == "") {
            CM.dropBeforeTake();
        }
    };

    this.animateDisabledDeviceNotification = function(customPopups) {
        animateDisabledDeviceNotification(customPopups);
    };

    this.animateAllowStreamNotification = function(customPopups) {
        animateAllowStreamNotification(customPopups);
    };

    this.animateClosingAllowStreamNotification = function() {
        animateClosingAllowStreamNotification();
    };

    _this.pubs = {
        videopub : '<div id="video_pub" style="top: 0; background-color: black; display: inline-block; position: absolute; margin-right: 30px; z-index: 9999;" />',
        audiopub : '<div id="audio_pub" style="top: 0; background-color: black; display: inline-block; position: absolute; margin-right: 30px; z-index: 9999;" />',
        videosub : '<div id="video_sub" style="width:449px; height:336px; display:inline-block; top:0;"/>',
        audiosub : '<div id="audio_sub" style="display:block; position:absolute; width:30px; height:30px; top:5px; left:5px"/>'
    }

}


function WSReceived() {
    this.startTyping = function(event){
        if (event.arguments.id == CM.request.currentRequest.requestId) {
            CM.consultationWindow.find('.modal-body').addClass('opponent-typing');
            CM.autoResizeHeight();
        }
    };
    this.stopTyping = function(event){
        if (event.arguments.id == CM.request.currentRequest.requestId) {
            CM.consultationWindow.find('.modal-body').removeClass('opponent-typing');
            CM.autoResizeHeight();
        }
    };
    this.sendMessage = function(event){
        if (event.arguments.request_id == CM.request.currentRequest.requestId) {
            CM.request.currentRequest.unread++;
            if (CM.consultationWindow.hasClass('fullscreen')) {
                CM.consultationWindow.find('.action-buttons').find('.messages').addClass('new');
            }
            message = event['result']['message'].replace(/(\r\n|\n|\r)/gm, "<br>");

            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);

            appendingHTML = CM.appendMessage('opponent', event['event_originator_id'], message, getFormattedDateTime(new Date(timestamp)), 'unread');
            CM.addHTMLToChat(appendingHTML);
        } else {
            if (typeof CM.addUnreadToRequestsList === 'function') {
                CM.addUnreadToRequestsList(event.arguments.request_id);
            }
        }
        $('.message').linkify();
    };
    this.markAsRead = function(event) {
        CM.consultationWindow.find('.me.unread').removeClass('unread');
    };
}

var CallModel = function() {
    this.openTok = new OpenTok(this);

    this.wsReceived = new WSReceived();

    this.callState = 'inactive';

    this.myId = '';

    this.sendingMessage = false;
    this.tempMessage = '';

    this.callWindowIsFullscreen = function(){
        if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement) {
            return true;
        } else {
            return false;
        }
    };

    this.toggleFullscreen = function() {
        if (this.callWindowIsFullscreen()) {
            this.exitFullscreen();
        } else {
            if (CM.request.currentRequest.callActive) {
                this.launchIntoFullscreen();
            }
        }
    };

    this.launchIntoFullscreen = function() {
        element = CM.consultationWindow.find('.modal-body')[0];
        if(element.requestFullscreen) {
            element.requestFullscreen();
        } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if(element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    };

    this.exitFullscreen = function() {
        if(document.exitFullscreen) {
            document.exitFullscreen();
        } else if(document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    };

    this.getBrowser = function(){
        var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if (M[1]=== 'Chrome') {
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if ((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    };

    this.browser = this.getBrowser().split(' ')[0].toLowerCase();

    this.callUI = {
        callPopupVisible : false,
        consultationWindow : false,
        callCirclesIterator : 0
    };

    this.request = {
        tableData : [],
        requests : [],
        currentRequest : new Request()
    };

    this.sendDrop = function(async) {
        window.onbeforeunload=null;
        if(typeof(async)==='undefined') async = true;
        var _this = this;
        var arguments = {};
        arguments['conversation_id'] = this.request.currentRequest.conversationId;
        $.ajax({
            async:async,
            url: '/mobile/conversation/drop/',
            data: arguments,
            success: function(result) {
                _this.dropCall(result);
            }
        });
    };

    this.hideCallPopup = function() {
        $('#callpoup').modal('hide');
        $('#callpoup .incomigAudioCall').css('display', 'none');
        $('#callpoup .incomigVideoCall').css('display', 'none');
        this.callUI.callPopupVisible = false;
        this.callState = 'inactive';
    };

    this.dropBeforeTake = function() {
        CM.sendDrop();
        CM.localDrop();
        CM.hideCallPopup();
    };

    this.talkingTimer = function() {
        var currentTime = new Date().getTime();
        var talkingTime = currentTime - this.request.currentRequest.callStartTime;
        var hours = Math.floor( talkingTime / (1000*60*60) );
        talkingTime -= hours*1000*60*60;
        var minutes = Math.floor(talkingTime/(1000*60));
        talkingTime -= minutes*1000*60;
        var seconds = Math.floor(talkingTime/1000);
        $('.duration').text( pad(hours, 2) + " : " + pad(minutes,2) + " : " + pad(seconds, 2));

        var _this = this;
        if (this.request.currentRequest.callActive) {
            setTimeout(function() { _this.talkingTimer(); }, 500);
        } else {
            return;
        }
    };

    this.callAnimation = function() {
        var _this = this;
        if (this.callUI.callCirclesIterator >= 5) {
            this.callUI.callCirclesIterator = 0;
            $('.spot').removeClass('big').removeClass('pink')
        }
        $.each(jQuery(".spots-cont .spot"), function (index, element) {
            if (index === _this.callUI.callCirclesIterator) {
                $(this).addClass('big').addClass('pink');
            }
            else {
                $(this).removeClass('big').removeClass('pink')
            }
        });

        this.callUI.callCirclesIterator++;

        if (this.callUI.callPopupVisible) {
            setTimeout(function() {_this.callAnimation(); }, 1000)
        }
    };

    this.getChat = function() {
        CM.request.currentRequest.medcardLoaded = false;
        $('#showChat').trigger('click');
        getData = {};
        getData.request_id=this.request.currentRequest.requestId;

        var _this = this;
        $.get('/mobile/user/get_log', getData, function(data) {
            if (data.status != "OK") {
                console.log(data.errors);
            } else {
                $(".chat-messages").empty();
                $(".conclusion-messages").empty();
                forEach(data.log, function(key, log) {
                    _this.addLogToChat(log);
                });
                _this.request.currentRequest.requestOpened = data.opened;
                _this.request.currentRequest.resolution = data.resolution;
                _this.request.currentRequest.unread = data.request_unread_count;
                $('.message').linkify();
                updateScrollbar($('#chat-container'));
                $('.image-attachment').load(function(){
                    updateScrollbar($('#chat-container'));
                });
                $('.chat-messages img').load(function(){
                    updateScrollbar($('#chat-container'));
                });
                _this.setMessagingFormState();
            }
        }, 'json');


    };

    this.setMessagingFormState = function() {
        var form = CM.consultationWindow.find('.messaging-form');
        var input = form.find('.input-msg');
        if (this.request.currentRequest.requestOpened == 'true') {
            form.removeClass('disabled');
            input.removeAttr('disabled');
        } else {
            form.addClass('disabled');
            input.attr('disabled', 'disabled');
            if (typeof this.showResolutionPopup === "function") {
                if (typeof this.request.currentRequest.resolutionNeeded !== 'undefined'
                    && this.request.currentRequest.resolutionNeeded == '1'
                    && !this.request.currentRequest.resolution
                    && this.request.currentRequest.resolution != '')
                {
                    this.showResolutionPopup();
                }
            }
        }
    };

    this.addToChat = function(log, opp_id, object_id) {
        var author_class = 'me';
        if (log['author_id'] == opp_id) {
            author_class = 'opponent';
        }
        var message = log['message'].replace(/(\r\n|\n|\r)/gm, "<br>");
        appendingHTML = '';

        switch (log['action']) {
            case 'message_sent':
                appendingHTML = this.appendMessage(author_class, log['author_id'], message, log['creation_time'], log['status']);
                break;
            case 'system':
                appendingHTML = this.appendSystemMessage(message, log['creation_time']);
                break;
            case 'attachment_added':
                appendingHTML = this.appendAttachment(author_class, log['author_id'], message, log['creation_time'], log['status'], log['object_id']);
                break;
            case 'conversation_initiated':
                if (log['message'] == 'sip') {
                    appendingHTML = this.appendCallStarted(log['creation_time'], 'SIP', log['status'], log['object_id']);
                }
                break;
            case 'conversation_taken':
                if (log['message'] == 'sip') {

                } else {
                    appendingHTML = this.appendCallStarted(log['creation_time'], '', log['status'], log['object_id']);
                }
                break;
            case 'conversation_dropped':
                status = (log['author_id'] == this.myId ? '' : log['status']);
                appendingHTML = this.appendCallDropped(log['creation_time'], status);
                break;
            case 'conversation_dropped_by_system':
                appendingHTML = this.appendCallDroppedSystem(log['creation_time'], log['status']);
                break;
            case 'conversation_missed':
                status = (log['author_id'] == this.myId ? '' : log['status']);
                appendingHTML = this.appendCallMissed(log['creation_time'], status);
                break;
            case 'conversation_busy':
                appendingHTML = this.appendCallBusy(log['creation_time'], log['status']);
                break;
            case 'conversation_finished':
                status = (log['author_id'] == this.myId ? '' : log['status']);
                appendingHTML = this.appendCallFinished(log['creation_time'], status);
                break;
            case 'request_canceled':
            case 'request_solved':
                appendingHTML = this.appendRequestSolved(log['creation_time'], log['status']);
                break;
            case 'money_refund':
                appendingHTML = this.appendMoneyRefund(log['creation_time'], log['status']);
                break;
            case 'request_canceled_by_admin':
            case 'request_canceled_by_doctor':
                appendingHTML = this.appendRequestCanceledDoctor(log['creation_time'], log['status']);
                break;

            case 'request_canceled_by_user':
                appendingHTML = this.appendRequestCanceledUser(log['creation_time'], log['status']);
                break;
            case 'conclusion_added':
                status = (log['author_id'] == this.myId ? '' : log['status']);
                appendingHTML = this.appendConclusion(author_class, log['author_id'], message, log['creation_time'], status);
                this.request.currentRequest.conclusionExists = true;
                if (this.appendConclusionToConclusions) {
                    appendingHTMLConclusions = this.appendConclusionToConclusions(message, log['creation_time'], log['status']);
                    $('#conclusions-container .conclusions').append(appendingHTMLConclusions);
                    updateScrollbar($('#conclusions-container'));
                }
                break;
            case 'request_initiated':
                appendingHTML = this.appendInitHTML(log['creation_time']);
                if (this.appendConclusionInitHTML) {
                    appendingHTMLConclusions = this.appendConclusionInitHTML(log['creation_time']);
                    $('.conclusion-messages').append(appendingHTMLConclusions);
                    updateScrollbar($('#conclusionMessages'));
                }
                break;
            default:
                //console.log('unknown action for log with id='+log['id']+', action is '+log['action']);
        }
        $('.chat-messages').append(appendingHTML);
    };

    this.appendCallStarted = function(time, sip, status) {

        messageClass = (status == 'unread' ? ' unread' : '');
        if (typeof sip === 'undefined'){
            sip='';
        }
        text = (sip == 'SIP' ? '<span class="text">Начало звонка на мобильный</span>' : '<span class="text">Начало звонка</span>');
        htmlText = '' +
            '<div class="system-notification call-initiated' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        text +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendCallDropped = function(time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification call-dropped ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Звонок отклонен</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendCallDroppedSystem = function(time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification call-dropped ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Звонок прерван</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendCallMissed = function(time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification call-missed ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Звонок пропущен</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendCallBusy = function(time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification call-missed ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Пользователь занят</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendRequestSolved = function(time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification req-solved ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Консультация закрыта</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendCallFinished = function(time, status, sip) {
        if (typeof sip === 'undefined'){
            sip='';
        }
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification call-finished ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Звонок '+sip+' завершен</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendMoneyRefund = function(time) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification money-refund ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Средства возвращены на Ваш баланс</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendPromoRefund = function(time) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification money-refund ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Промо-код выслан на эл. почту</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendRequestCanceledUser = function(time) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification calendar-blue ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Консультация отменена пользователем</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendRequestCanceledDoctor = function(time) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification calendar-red ' + messageClass + '">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Консультация отменена врачом</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendMessage = function(author_class, author_id, message, time, status) {
        messageClass = '';
        if (time == '') messageClass += ' tempMessage';
        if ($.trim(message) == '') message = '&nbsp;';
        if (status == 'unread') messageClass += ' unread';
        htmlText = '' +
            '<div class="message '+author_class+messageClass+'">' +
                '<div class="avatar-cont">' +
                    '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="text">' +
                        message +
                        '<span class="datetime"> <span class="date"> - '+getLiteralDate(time)+'</span><i class="seen-status"></i></span>' +
                        '<i class="seen"></i>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';

        return htmlText;
    };

    this.appendSystemMessage = function(message, time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="message system ' + messageClass + '">' +
                '<div class="avatar-cont">' +
                    '<img src="/images/logo-face.png">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="text">' +
                        message +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';

        return htmlText;
    };

    this.appendAttachment = function(author_class, author_id, message, time, status, object_id) {
        messageClass = '';
        if (time == '') messageClass += ' tempMessage';
        if ($.trim(message) == '') message = '&nbsp;';
        messageClass += (status == 'unread' ? ' unread' : '');
        switch (message) {
            case 'bmp':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/bmp-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'tiff':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/tiff-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'pdf':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/pdf-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'doc':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/doc-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'docx':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/doc-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'xls':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/xls-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'xlsx':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/xls-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            default:
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock image-attachment" style="background-image: url(\'/mobile/user/get_request_attachment/?id='+object_id+'\');" onclick="ShowAttachment('+object_id+')">' +
                            '<div class="text-container">' +
                                '<span class="datetime">'+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
        }

        return htmlText;
    };

    this.appendConclusion = function(author_class, author_id, message, time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification conclusion ' + messageClass + '">' +
                '<div class="avatar-cont">' +

                '</div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Доктор добавил заключение</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        htmlText += '' +
            '<div class="system-notification opponent conclusion-detailed ' + messageClass + '">' +
                '<div class="avatar-cont">' +
                    '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="cont">' +
                        '<div class="conclusion-wrapper">' +
                        '<span class="conclusion-title">Заключение</span>' +
                        '<span class="conclusion-text">' + message + '</span>' +
                            '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendInitHTML = function(time) {

        htmlText = '' +
            '<div class="system-notification req-created {$status}">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Начало консультации</span>' +
                        '<span class="datetime"> - ' + getLiteralDate(time) + '</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.appendConclusionInitHTML = function(time) {
        htmlText = '' +
            '<div class="req-created-concl">' +
                '<i></i>' +
                '<span class="req-created-concl-text">Заключения <span class="date">'+getLiteralDate(time)+'</span></span>' +
            '</div>' +
            '<hr>' +
        '';
        return htmlText;
    };

    this.createConnection = function(object) {

        CM.consultationWindow.find('.modal-body').addClass('inCall')
            .find('.chat-wrapper').removeClass('wide').end()
            .find('.send-call').addClass('drop-call');

        updateScrollbar($('#chat-container'));

        this.openTok.token = object['token'];
        this.openTok.sessionId = object['session'];

        this.openTok.publishVideo = this.openTok.streamVideo;
        CM.consultationWindow.find('.share-video').removeClass('not-active').end()
            .find('.share-audio').removeClass('not-active');
        if (this.openTok.streamVideo) {
            CM.consultationWindow.find('.share-video').removeClass('disabled');
        } else {
            CM.consultationWindow.find('.share-video').addClass('disabled');
        }
        CM.consultationWindow.find('.video-wrapper').addClass('visible');

        this.openTok.publishAudio = true;
        CM.consultationWindow.find('.share-audio').removeClass('disabled');


        switch (this.request.currentRequest.requestType) {
            case 'audio':
                this.openTok.pubOptions = {publishAudio: true, publishVideo: false, width: 120, height: 95};
                this.openTok.initOpentok();
                this.talkingTimer();
                if( typeof this.updateConversation == 'function') {
                    this.updateConversation();
                }
                break;
            case 'text':
                break;
            default :
                this.openTok.setPubOptions({publishAudio: true, publishVideo: this.openTok.streamVideo, width: 120, height: 95});
                this.openTok.initOpentok();
                this.talkingTimer();
                if( typeof this.updateConversation == 'function') {
                    this.updateConversation();
                }
                break;
        }
    };

    this.unload = function(){
        return "Вы находитесь в процессе консультации ,Если вы закроете браузер связь прервется";
    };

    this.autoResizeHeight = function(ele) {
        ele.style.height = 'auto';
        var newHeight = Math.ceil((ele.scrollHeight + 1) / 32 ) * 32;
        ele.style.height = newHeight.toString() + 'px';

        chatContHeight = 395;
        if (CM.consultationWindow.find('.modal-body').hasClass('opponent-typing')) {
            chatContHeight = 375;
        }

        var chatContainerNewHeight = (newHeight <= 128 ? chatContHeight - newHeight + 32 : 299);

        $('#chat-container').height(chatContainerNewHeight);
        updateScrollbar($('#chat-container'));
    };

    this.markRequestAsRead = function(request_id) {
        if (CM.request.currentRequest.markAsReadSending){
            return;
        }
        CM.request.currentRequest.markAsReadSending = true;
        var arguments = {};
        arguments['request_id'] = request_id;
        $.ajax({
            url: '/mobile/user/mark_as_read/',
            type: 'GET',
            data: arguments,
            success: function (data) {
            //    if (typeof data.marked !== 'undefined' && data.marked > 0) {
                    setTimeout(function(){
                        CM.request.currentRequest.markAsReadSending = false;
                        unreadBage = $("#history_unread_count");
                        unread = parseInt(unreadBage.text()) - data.marked;
                        if (unread < 0) unread = 0;
                        if (isNaN(unread)) unread = 0;
                        if (unread==0) unread = '';

                        $('.message.opponent.unread').removeClass('unread');
                        $('.system-notification.unread').removeClass('unread');
                        unreadBage.text(unread);

                        $('.request-unread-count').each(function(){
                            if ($(this).data("id") == request_id) {
                                $(this).text('');
                            }
                        });

                        i=0;
                        forEach(CM.request.requests, function(key, value) {

                            if (value.requestId == request_id) {
                                if (CM.pageAction == 'serving') {
                                    if ($('#paientsTable').length > 0) {
                                        $('#paientsTable').dataTable().fnUpdate('<div class="user-name">'+value.userName+'<span class="badge-wrapper"><span class="badge request-unread-count" data-id="'+request_id+'"></span></span></div>', i, 2, false);
                                    }
                                }
                            }

                            i++;
                        });

                        if (CM.callUI.consultationWindow && CM.request.currentRequest.requestId == request_id) {
                            CM.consultationWindow.find('.opponent.unread').removeClass('unread');
                            CM.request.currentRequest.unread = 0;
                        }

                        if (unread == 0) {
                            setTimeout(function(){unreadBage.text('');}, 1000);
                        }
                    }, 1000);
             //   }
            },
            error: function(er){
                CM.request.currentRequest.markAsReadSending = false;
                console.log('could not mark as read');
            }
        });
    };

    this.sendTypingStatus = function(isTyping) {

        requestUrl = '/mobile/user/stop_typing/';
        if (isTyping) {
            requestUrl = '/mobile/user/start_typing/';
        }

        $.ajax({
            url: requestUrl,
            data: {request_id: this.request.currentRequest.requestId},
            success: function(data) {

            },
            error: function() {

            }
        })

    };

    this.emptyCallWindow = function() {

        CM.consultationWindow
            .find('.modal-body').removeClass('inCall').removeClass('incomingCall').removeClass('outgoingCall').end()
            .find('.incoming-call-wrapper').removeClass('sending-action')
                .find('.call-title .text').text('Входящий звонок').end()
            .end()
            .find('.video-wrapper').removeClass('visible').end()
            .find('.chat-messages').empty().end()
            .find('.conclusions').empty().end()
            .find('#medcard-container .medcard').empty().end()
            .find('#consultationWindowHeader')
                .find('.avatar').attr('src', '/images/avatars/doctor/male/userpic100.jpg').end()
                .find('.name').text('').end()
                .find('.profession').text('').end()
            .end();

        updateScrollbar($('#chat-container'));
        CM.consultationWindow.find('.send-message-wrapper .input-msg').val('');

        this.request.currentRequest = new Request();
        this.openTok.session = null;
        this.openTok.sessionId = null;
        this.openTok.token = null;
        this.openTok.publisher = null;
        this.openTok.subscriber = null;
        this.openTok.stream = null;
        this.openTok.requestType = null;
        this.openTok.pubOptions = null;
        this.openTok.streamVideo = null;
    };

    this.showSentMessage = function(result) {
        var messageSentSeen = false;
        var message = result['message'].replace(/(\r\n|\n|\r)/gm, "<br>");
        if(CM.consultationWindow.css('display') == 'block') {

            timestamp = (typeof result['unix_timestamp'] == 'undefined' ? new Date() : parseInt(result['unix_timestamp'])*1000);
            appendingHTML = this.appendMessage('me', $('#myId').val(), message, getFormattedDateTime(new Date(timestamp)), 'unread');
            $('.chat-messages').append(appendingHTML);
            updateScrollbar($('#chat-container'));
            CM.consultationWindow.find('.modal-body').removeClass('opponent-typing');
            messageSentSeen = true;
        }
        $('.message').linkify();
    };

    this.toggleOptionsState = function () {
        modalBody = CM.consultationWindow.find('.modal-body');
        if (modalBody.hasClass('options-shown')) {
            modalBody.removeClass('options-shown');
        } else {
            TestConnection();

            modalBody.addClass('options-shown');

            if (testManager.testModuleResults.ws === null || testManager.testModuleResults.webRTC === null) {
                $('#optionsTestConnection').removeClass('error').removeClass('ok').addClass('unknown');
            } else if (testManager.testModuleResults.ws && testManager.testModuleResults.webRTC) {
                $('#optionsTestConnection').removeClass('error').removeClass('unknown').addClass('ok');
            } else {
                $('#optionsTestConnection').removeClass('ok').removeClass('unknown').addClass('error');
            }

            if (testManager.testModuleResults.video === null) {
                $('#optionsTestCamera').removeClass('error').removeClass('ok').addClass('unknown');
            } else if (testManager.testModuleResults.video) {
                $('#optionsTestCamera').removeClass('error').removeClass('unknown').addClass('ok');
            } else {
                $('#optionsTestCamera').removeClass('ok').removeClass('unknown').addClass('error');
            }

            if (testManager.testModuleResults.audio === null) {
                $('#optionsTestMicrophone').removeClass('error').removeClass('ok').addClass('unknown');
            } else if (testManager.testModuleResults.audio) {
                $('#optionsTestMicrophone').removeClass('error').removeClass('unknown').addClass('ok');
            } else {
                $('#optionsTestMicrophone').removeClass('ok').removeClass('unknown').addClass('error');
            }

            if (testManager.testModuleResults.sound === null) {
                $('#optionsTestSpeakers').removeClass('error').removeClass('ok').addClass('unknown');
            } else if (testManager.testModuleResults.sound) {
                $('#optionsTestSpeakers').removeClass('error').removeClass('unknown').addClass('ok');
            } else {
                $('#optionsTestSpeakers').removeClass('ok').removeClass('unknown').addClass('error');
            }

            CM.consultationWindow.find('.options-menu-wrapper li:first').trigger('click');
        }

    };

    this.addHTMLToChat = function(appendingHTML) {
        $('.chat-messages').append(appendingHTML);
        updateScrollbar($('#chat-container'));
    }

};
