function Sip(CM) {

    var _this = this;

    this.init=function(){
        $('body').append('<video id="sipaudio"></video>')
    };

    this.callsound = new Audio('/images/ringbacktone.wav');
    this.session = null;
    this.state = 'normal';

    this.userAgent = new SIP.UA({
        uri: '1000@sip.pediatr247.ru',
        wsServers: ['wss://sip.pediatr247.ru:7443/'],
        authorizationUser: '1000',
        password: 'pedisip'
    });

    this.mediaConstraints = {
        audio: true,
        video: false
    };

    this.startCall = function (phone,conversation_id,environment) {
        askingToAllow = true;
        setAchtungText('Требуется доступ к микрофону');
        setTimeout(function(){
            if (askingToAllow) {
                animateAllowStreamNotification();
            }
        }, 3000);
        this.session = this.userAgent.invite(phone+'_'+environment+'_'+conversation_id, {
            media: {
                constraints: this.mediaConstraints,
                render: {
                    remote: {
                        video: document.getElementById('remoteVideo')
                    }
                }
            }
        });
        this.session.on('connecting', function () {
            animateClosingAllowStreamNotification();
            _this.callsound.loop=true;
            _this.callsound.play();
        });
        this.session.on('accepted', function () {
            _this.callsound.pause();
            _this.setSipState('incall');
        });
        this.session.on('bye', function () {
            _this.callsound.pause();
            _this.session = null;
            _this.dropSip();
        });
        this.session.on('failed', function (e) {
            _this.callsound.pause();
            _this.session = null;
            _this.dropSip();
        });
        this.session.on('cancel', function () {
            _this.callsound.pause();
            _this.session = null;
            _this.dropSip();
        });
    };

    this.cancelCall = function() {
        this.session.cancel();
    };

    this.dropCall = function() {
        this.session.bye();
    };

    this.dropSip = function() {
        window.onbeforeunload = null;
        animateClosingAllowStreamNotification();
        CM.sendDrop();
        CM.localDrop();
    };

    this.setSipState = function(state) {
        var sipButton = $('.send-call-sip');
        switch (state) {
            case 'connecting':
                sipButton.removeClass('in-call').addClass('sending');
                this.state = 'connecting';
                break;
            case 'disconnecting':
                sipButton.removeClass('in-call').addClass('sending');
                this.state = 'disconnecting';
                break;
            case 'normal':
                sipButton.removeClass('sending').removeClass('in-call');
                this.state = 'normal';
                break;
            case 'incall':
                sipButton.removeClass('sending').addClass('in-call');
                this.state = 'incall';
                break;
            default:
                console.log('unknown state of sip');
        }
    };

}

var DoctorWSReceived = function() {
    this.fiveMinutesLeft = function(event){
        if (CM.callUI.consultationWindow && (CM.request.currentRequest.requestId != event.arguments.request_id)) {
            if(event.result.next_slot) {
                PopupSuccess('У Вас назначена консультация, которая начнется через 5 минут.', 'Внимание!');
            }
        }
    };
    this.addAttachment = function(event){
        if (event.arguments.request_id == CM.request.currentRequest.requestId) {
            CM.request.currentRequest.unread++;
            message = event['result']['description'].replace(/(\r\n|\n|\r)/gm, "<br>");
            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
            appendingHTML = CM.appendAttachment('opponent', event['event_originator_id'], message, getFormattedDateTime(new Date(timestamp)), 'unread', event['result']['attachment']);
            CM.addHTMLToChat(appendingHTML);
        } else {
            CM.addUnreadToRequestsList(event.arguments.request_id);
        }
    };
    this.answerAsChat = function(event){
        PopupSuccess('Пользователь пожелал продолжить общение перепиской.');
        appendingHTML = '';
        if (event.arguments.request_id == CM.request.currentRequest.requestId) {
            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
            appendingHTML = CM.appendMessage('opponent', event['event_originator_id'], message, getFormattedDateTime(new Date(timestamp)), 'unread');
            if (CM.consultationWindow.hasClass('fullscreen')) {
                CM.consultationWindow.find('.action-buttons').find('.messages').addClass('new');
            }
        }
        CM.addHTMLToChat(appendingHTML);
    };
    this.takeCall = function(event){
        if (event.result.request_id == CM.request.currentRequest.requestId) {
            if (CM.callState == 'calling') {
                $('.beep').remove();
                if (CM.consultationWindow.hasClass('fullscreen')) {
                    CM.consultationWindow.find('.action-buttons').find('.messages').addClass('new');
                }
                CM.consultationWindow.modal('show');
                CM.hideCallPopup();
                CM.callUI.consultationWindow = true;
                CM.request.currentRequest.callStartTime = new Date().getTime();
                CM.openTok.streamVideo = true;
                CM.createConnection(event['result']);
            }
            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
            if(event['result']['sip']){
                appendingHTML = CM.appendCallStarted(getFormattedDateTime(new Date(timestamp)), 'SIP');
                window.onbeforeunload = function(){
                    CM.dropSip()
                };
            } else {
                appendingHTML = CM.appendCallStarted(getFormattedDateTime(new Date(timestamp)));
                window.onbeforeunload = CM.unload;
            }
            CM.addHTMLToChat(appendingHTML);
        }
    };
    this.userOnline = function(event){
        var glittertitle = ' ';
        var glittertext = '';
        glittertitle = glittertitle + event['result']['request'][0]['user_first_name'] + ' вошел(ла) в систему. Свяжитесь с пациентом для завершения консультации.';
        $.gritter.add({
            title: glittertitle,
            text: glittertext,
            image: '/mobile/user/userpic/'+event['result']['request'][0]['user_id']+'/avatar/'
        });
        $('.gritter-item .requests').perfectScrollbar({
            wheelSpeed: 1,
            wheelPropagation: true,
            suppressScrollX: true
        });
    };
    this.sendNotification = function(event){
        if(event.result.object_id != CM.request.currentRequest.requestId) {
            $('.notification_sound').remove();
            $(".ico").append('<span id="notification_count" class="badge pink"></span>');
            appendingHTML = '<audio class="notification_sound" src="/images/notification.wav" width="1px" height="1px" autoplay onended="RemoveNotification()"></audio>';
            $(document.body).append(appendingHTML);
            notificationBadge = $("#notification_count");
            if(parseInt(notificationBadge.text())) {
                notificationBadge.text( parseInt(notificationBadge.text())+1);
            } else {
                notificationBadge.text('1');
            }
            notifHTML = CM.addNotificationBlock(event['result']);
            $('.notifs_cont').prepend(notifHTML);
        }
    };
    this.startCall = function(event){
        CM.updateOndutyBadge();
    };
    this.dropCall = function(event){
        if(event.result.conversation_id == CM.request.currentRequest.conversationId
            || event.result.result.conversation_id == CM.request.currentRequest.conversationId) {
            if (CM.consultationWindow.hasClass('fullscreen')) {
                CM.consultationWindow.find('.action-buttons').find('.messages').addClass('new');
            }
            CM.hideCallPopup();
            CM.localDrop();
            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
            status = (event.event_originator_id == CM.myId ? '' : 'unread');
            //status = 'unread';
            appendingHTML = '';
            switch (event.result.status) {
                case 'dropped':
                    if (event.event_originator_id == CM.myId) {
                        appendingHTML = CM.appendCallMissed(getFormattedDateTime(new Date(timestamp)), status);
                    } else {
                        appendingHTML = CM.appendCallDropped(getFormattedDateTime(new Date(timestamp)), status);
                    }
                    break;
                case 'missed':
                    appendingHTML = CM.appendCallMissed(getFormattedDateTime(new Date(timestamp)), status);
                    break;
                case 'finished':
                    appendingHTML = CM.appendCallFinished(getFormattedDateTime(new Date(timestamp)), status);
                    break;
                default:
                    appendingHTML = CM.appendCallDroppedSystem(getFormattedDateTime(new Date(timestamp)), status);
            }
            CM.addHTMLToChat(appendingHTML);
            if (status == 'unread') {
                CM.request.currentRequest.unread++;
            }
        }
    };

    this.createOnDuty = function(event){
        appendingHTML = '<audio class="notification_sound" src="/images/notification.wav" width="1px" height="1px" autoplay onended="RemoveNotification()"></audio>';
        $(document.body).append(appendingHTML);
        CM.incrementBadgeCount('on_duty_count', 1);
    };
    this.createChosen = function(event){
        appendingHTML = '<audio class="notification_sound" src="/images/notification.wav" width="1px" height="1px" autoplay onended="RemoveNotification()"></audio>';
        $(document.body).append(appendingHTML);

        if (event['result']['status'] == 'OK') {
            reqDateStr  = event.result.request.appointment_time.split(' ');
            reqDate  = reqDateStr[0].split('-');
            if (reqDate[0] == curr_year && reqDate[1] == curr_month && reqDate[2] == curr_date) {
                CM.incrementBadgeCount('calendar_count', 1);
            }
        }
    };
};

var DoctorCallModel = function() {

    this.pageAction = '';

    this.sip = new Sip(this);

    this.wsReceived = new DoctorWSReceived();

    this.openTok.startRecording = function(){
        getData = {};
        getData.conversation_id = CM.request.currentRequest.conversationId;
        $.get('/mobile/user/start_recording/', getData, function(data){});
    };

    this.localDrop = function() {
        window.onbeforeunload=null;
        this.openTok.disconnectStuff();
        this.openTok.animateClosingAllowStreamNotification();

        this.sip.setSipState('normal');

        this.exitFullscreen();

        CM.consultationWindow.find('.modal-body').removeClass('incomingCall').removeClass('outgoingCall').removeClass('inCall')
            .find('.call-button').removeClass('drop-call').removeClass('disabled').end()
            .find('.video-wrapper').removeClass('visible').end()
            .find('.chat-wrapper').addClass('wide').end()
            .find('.share-video').addClass('not-active').addClass('disabled').end()
            .find('.share-audio').addClass('not-active').addClass('disabled').end()
            .find('.outgoing-call-wrapper .drop-call').removeClass('active').end();

        updateScrollbar($('#chat-container'));

        $('.beep').remove();
        if (CM.sip.session) {
            CM.sip.callsound.pause();
            CM.sip.session = null;
            CM.sip.dropSip();
        }

        this.request.currentRequest.callActive = false;
        return true;
    };

    this.sendStart = function() {
        this.request.currentRequest.callActive = true;
        if($('.in-call-button.call-button.send-call').hasClass('disabled')) {
            return;
        }
        $('.send-call-sip').addClass('disabled');
        var _this = this;
        var arguments = {};

        arguments['request_id'] = this.request.currentRequest.requestId;
        $(document.body).append('<audio class="beep" width="1px" height="1px" autoplay loop src="/images/beep.ogg"></audio>');
        $.ajax({
            url: '/mobile/conversation/start/',
            data: arguments,
            success: function(result) {
                if (result.status=='error') {
                    if (result.errors=='too_soon_to_start') {
                        _this.hideCallPopup();
                        _this.localDrop();

                        PopupError('Вы не можете звонить сейчас, до начала консультации еще более 10 минут.');
                    }
                    if (result.errors=='invalid_request_id_or_appointment_id') {
                        _this.hideCallPopup();
                        _this.localDrop();

                        PopupError('Заявка не найдена');
                        //CM.request.requests.splice(0, 1);
                        //$('#paientsTable').dataTable().fnDeleteRow(0);
                    }
                    if (result.errors=='user_busy') {
                        _this.hideCallPopup();
                        _this.localDrop();

                        PopupError('Пользователь занят.');
                    }
                } else {
                    window.onbeforeunload=CM.unload;
                    CM.getChat();
                    _this.sentStartStuff(result);
                }
            }
        });

        //WebSocketLocal.RequestCommand('/mobile/conversation/start/', arguments);
    };

    this.incrementWaitingTime = function() {
        for(var i = 0; i < this.request.requests.length; i++) {
            this.request.requests[i].waitTime++;
            var time = '';
            time = secondsToTime(this.request.requests[i].waitTime);
            if ($('#paientsTable').length > 0) {
                $('#paientsTable').dataTable().fnUpdate(time, i, 1, false);
            }
        }
    };

    this.parentInfo = function() {
        postData = {};
        postData.id = this.request.currentRequest.userId;

        $.post('/mobile/user/get_user_info/', postData, function (data) {
            forEach(data, function (key, value) {
                if (document.getElementById(key)) {
                    $('#' + key).text(value);
                }
            });
            $("#loadingImg2").hide();
        }, 'json');
    };

    this.addMessage = function(message)  {

        if (this.sendingMessage) return;

        updateScrollbar($('#chat-container'));
        textArea = $(".messaging-form textarea");
        textArea.text('');

        if ($.trim(message) != '') {
            CM.sendingMessage = true;
            CM.tempMessage = message;
            CM.consultationWindow.find('.messaging-form').attr('title', 'Вы сможете отправить следующее сообщение, когда предыдущее будет обработано.');

            appendingHTML = this.appendMessage('me', $('#myId').val(), 'Сообщение отсылается', '', '');
            $('.chat-messages').append(appendingHTML);
            updateScrollbar($('#chat-container'));
            if (this.request.currentRequest.requestId === null || typeof this.request.currentRequest.requestId =='undefined') {
                PopupError('There is no active consultation to add conclusion');
            } else {
                var _this = this;
                var arguments = {};
                arguments['message'] = message;
                arguments['request_id'] = this.request.currentRequest.requestId;
                $.ajax({
                    url: '/mobile/conversation/send_message/',
                    data: arguments,
                    success: function(result) {
                        CM.sendingMessage = false;
                        CM.consultationWindow.find('.messaging-form').removeAttr('title').end()
                            .find('.tempMessage').remove().end()
                            .find('.input-msg').removeAttr('style').val(CM.tempMessage).trigger('keypress').end();
                        CM.tempMessage = '';

                        if (result['status'] != 'OK') {
                            switch (result['errors']) {
                                case 'request_closed':
                                    PopupError('Консультация закрыта․ Ваше сообщение не может быть отправлено.');
                                    break;
                                case 'missing_arguments':
                                    break;
                                case 'too_soon_to_answer':
                                    PopupError('Вы не можете ответить сейчас, до начала консультации еще более 10 минут.');
                                    break;
                                default :
                                    //TODO: create all possible cases with their error-alerting popups...
                                    console.log(result['errors']);
                            }
                        } else {
                            CM.sendingMessage = false;
                            CM.consultationWindow.find('.messaging-form').removeAttr('title').end()
                                .find('.tempMessage').remove().end()
                                .find('.input-msg').removeAttr('style').val('').trigger('keypress').end();
                            CM.tempMessage = '';

                            _this.showSentMessage(result);
                        }
                    },
                    error: function() {
                        CM.sendingMessage = false;
                        CM.consultationWindow.find('.messaging-form').removeAttr('title').end()
                            .find('.tempMessage').remove().end()
                            .find('.input-msg').removeAttr('style').val(CM.tempMessage).trigger('keypress').end();
                        CM.tempMessage = '';

                        PopupError('Ошибка связи с сервером, сообщение не отослано.');
                    }
                });
            }
        }
        return false;
    };

    this.addConclusion = function(conclusion) {
        if (conclusion.trim() != '') {
            if (this.request.currentRequest.requestId === null) {
             //   bootbox.alert("There is no active consultation to add conclusion");
                PopupError('There is no active consultation to add conclusion');

            } else {
                var submitButton = $('#addConclusion');

                if (submitButton.hasClass('sending')) return;

                var _this = this;
                var arguments = {};
                arguments['conclusion'] = conclusion;
                arguments['conversation_id'] = this.request.currentRequest.conversationId;
                arguments['request_id'] = this.request.currentRequest.requestId;

                submitButton.addClass('sending');

                $.ajax({
                    url: '/mobile/doctor/add_conclusion/',
                    data: arguments,
                    success: function(data) {
                        submitButton.removeClass('sending');
                        if (data['status'] != 'OK') {
                            switch (data['errors']) {
                                case 'request_closed':
                                    PopupError('Консультация закрыта․ Ваше заключение не может быть добавлено.');
                                    break;
                                case 'missing_arguments':

                                    break;
                                case 'too_soon_to_answer':
                                    PopupError('Вы не можете добавить заключение сейчас, до начала консультации еще более 10 минут.');
                                    break;
                                default :
                                    //TODO: create all possible cases with their error-alerting popups...
                                    console.log(data['errors']);
                            }
                        } else {
                            _this.request.currentRequest.conclusionExists = true;
                            _this.hideConclusionPopup();
                            _this.showAddedConclusion(data);
                        }
                    },
                    error: function() {
                        PopupError('Не удалось установить связь с сервером.');
                    }
                });
            }
        } else {
            PopupError('Вы забыли написать текст заключения.');
        }
        return false;
    };

    this.showCallPopup = function(userid) {
        CM.consultationWindow.find('.modal-body').removeClass('incomingCall').removeClass('inCall').removeClass('options-shown').addClass('outgoingCall')
            .find('.outgoing-call-wrapper .userUserpic').attr('src', '/mobile/user/userpic/'+userid+'/avatar/?'+new Date().getTime()).end();
         //   .find('.options-button').addClass('disabled').attr('title', 'Тестировать можно только вне активного звонка.').end();
        this.callUI.callPopupVisible = true;
        this.callState = 'calling';

        this.callAnimation();
        return false;
    };

    this.hideCallPopup = function() {
        CM.consultationWindow.find('.modal-body').removeClass('incomingCall').removeClass('inCall').removeClass('options-shown').removeClass('outgoingCall')
            .find('.outgoing-call-wrapper .userUserpic').attr('src', '');
        this.callUI.callPopupVisible = false;
        this.callState = 'inactive';
    };

    this.extendRequest = function() {

        if (this.sip.state != 'normal') return;
        if(this.request.currentRequest.callActive) {
            this.sendDrop();
            this.localDrop();
        }

        CM.consultationWindow.modal('hide');
        this.callUI.consultationWindow = false;
        this.emptyCallWindow();

        return false;
    };

    this.onCallClick = function() {
        if($('.in-call-button.call-button.send-call').hasClass('disabled')) {
            return;
        }
        if (!hasVideoDevice) {
            PopupError('Для консультирования пациентов вам необходимо наличие вэб камеры. Подсоедините камеру или обратитесь в нашу службу технической поддержки.');
            return false;
        }
        if (!this.request.currentRequest.callActive === true) {
            this.sendStart();
            this.showCallPopup(this.request.currentRequest.userId);
        }
        return false;
    };

    this.callSip = function(){
        if (!this.request.currentRequest.callActive === true && this.request.currentRequest.phone != '') {
            this.request.currentRequest.callActive = true;
            this.sip.setSipState('connecting');
            $('.in-call-button.call-button.send-call').addClass('disabled');
            var _this = this;
            var arguments = {};
            arguments['request_id'] = this.request.currentRequest.requestId;
            arguments['from_sip']=true;
            $.ajax({
                url: '/mobile/conversation/start/',
                data: arguments,
                success: function(result) {
                    if (result.status=='error') {
                        if (result.errors=='too_soon_to_start') {
                            _this.hideCallPopup();
                            _this.localDrop();

                            PopupError('Вы не можете звонить сейчас, до начала консультации еще более 10 минут.');
                            //CM.request.requests.splice(0, 1);
                            //$('#paientsTable').dataTable().fnDeleteRow(0);
                        }
                        if (result.errors=='user_busy') {
                            _this.hideCallPopup();
                            _this.localDrop();

                            PopupError('Пользователь занят.');
                            //CM.request.requests.splice(0, 1);
                            //$('#paientsTable').dataTable().fnDeleteRow(0);

                        }
                    } else {
                        _this.sip.init();
                        _this.sip.startCall(_this.request.currentRequest.phone,result.conversation_id,env);
                        $('#callSip').addClass('in-call');
                        _this.request.currentRequest.conversationId = result['conversation_id'];
                        $('#call').addClass('inactive');
                        window.onbeforeunload=CM.unload;
                    }
                }
            });
            return false;
        }
    };

    this.dropSip = function(){
        $('.in-call-button.call-button.send-call').removeClass('disabled');
        this.sip.setSipState('disconnecting');
        this.sip.dropCall();
    };

    this.onDropClick = function() {
        if (this.request.currentRequest.callActive === true) {
            this.sendDrop();
            this.localDrop();
        }
        return false;
    };

    this.addLogToChat = function(log) {
        this.addToChat(log, this.request.currentRequest.userId, this.request.currentRequest.userId, log.status)
    };

    this.showAddedConclusion = function(result) {

        $('.conclusion-form').find('textarea').val('');

        var message = result['conclusion'].replace(/(\r\n|\n|\r)/gm, "<br>");

        appendingHTML = '';
        timestamp = (typeof result['unix_timestamp'] == 'undefined' ? new Date() : parseInt(result['unix_timestamp'])*1000);
        appendingHTML = this.appendConclusion('me', $('#myId').val(), message, getFormattedDateTime(new Date(timestamp)), '');

        $('.chat-messages').append(appendingHTML);
        updateScrollbar($('#chat-container'));

        appendingHTMLConclusions = this.appendConclusionToConclusions(message, getFormattedDateTime(new Date(timestamp)));
        $('#conclusions-container .conclusions').append(appendingHTMLConclusions);
        updateScrollbar($('#conclusions-wrapper'));

    };

    this.appendConclusionToConclusions = function(message, time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification conclusion ' + messageClass + '">' +
                '<div class="avatar-cont">' +

                '</div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Доктор добавил заключение</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        htmlText += '' +
            '<div class="system-notification opponent conclusion-detailed ' + messageClass + '">' +
                '<div class="avatar-cont">' +

                '</div>' +
                '<div class="textblock">' +
                    '<div class="cont">' +
                        '<div class="conclusion-wrapper">' +
                        '<span class="conclusion-title">Заключение</span>' +
                        '<span class="conclusion-text">' + message + '</span>' +
                            '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.showConclusionPopup = function() {
        $('.existing-conclusions-wrapper').hide();
        $('.new-conclusions-wrapper').show();
    };

    this.hideConclusionPopup = function() {
        $('.new-conclusions-wrapper').hide();
        $('.existing-conclusions-wrapper').show();
    };

    this.dropCall = function(result) {
        this.openTok.disconnectStuff();
    };

    this.updateConversation= function(){
        getData = {};
        getData.conversation_id = CM.request.currentRequest.conversationId;
        var _this = this;
        $.get('/mobile/doctor/update_conversation/', getData, function (data) {
            if(CM.request.currentRequest.callActive) {
                setTimeout(function() { _this.updateConversation(); }, 30000);
            }

        }, 'json');

    };

    this.getConclusions = function(){};

    this.addUnreadToRequestsList = function(requestId, cnt) {

        if (typeof cnt === 'undefined') cnt = 1;

        unreadBage = $("#history_unread_count");
        unread = (unreadBage.text() == '' ? cnt : parseInt(unreadBage.text()) + cnt);
        if (unread < 0) unread = 0;
        if (isNaN(unread)) unread = 0;

        unreadBage.text(unread);
        oldCnt = parseInt($('.request-unread-count[data-id="'+requestId+'"]').text());
        if (isNaN(oldCnt)) oldCnt = 0;
        newCnt = oldCnt + cnt;

        for (i=0; i<this.request.requests.length; i++) {
            if (this.request.requests[0].requestId == requestId) {
                this.request.requests[0].unread += cnt;
            }
        }

        i=0;
        forEach(CM.request.requests, function(key, value) {

            if (value.requestId == requestId) {
                if (CM.pageAction == 'serving') {
                    if ($('#paientsTable').length > 0) {
                        $('#paientsTable').dataTable().fnUpdate('<div class="user-name">'+value.userName+'<span class="badge-wrapper"><span class="badge request-unread-count" data-id="'+requestId+'">'+newCnt+'</span></span></div>', i, 2, false);
                    }
                }
            }

            i++;
        });


    };

    this.updateOndutyBadge = function() {
        $.ajax({
            url: '/mobile/doctor/get_on_duty_count/',
            success: function (data) {
                if (data.status != 'OK') {
                    console.log(data.errors);
                } else {
                    $('#on_duty_count').text((data.count>0?data.count:''));
                }
            }
        })
    };

    this.incrementBadgeCount = function(badgeId, cnt) {
        var badge = $('#'+badgeId);
        if (badge.length == 0) return;
        if (typeof cnt === 'undefined') {
            cnt = 1;
        }
        newCnt = parseInt(badge.text()) + cnt;
        if (newCnt < 0) newCnt = 0;
        if (isNaN(newCnt)) newCnt = cnt;
        if (newCnt == 0) newCnt = '';
        badge.text(newCnt);
        badge.removeClass('css3-notification').addClass('css3-notification');
    };

    this.appendConclusion = function(author_class, author_id, message, time, status) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification conclusion ' + messageClass + '">' +
                '<div class="avatar-cont">' +

                '</div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Доктор добавил заключение</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        htmlText += '' +
            '<div class="system-notification opponent conclusion-detailed ' + messageClass + '">' +
                '<div class="avatar-cont">' +
                    '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="cont">' +
                        '<div class="conclusion-wrapper">' +
                        '<span class="conclusion-title">Заключение</span>' +
                        '<span class="conclusion-text">' + message + '</span>' +
                            '<span class="datetime"> - '+getLiteralDate(time)+'<i class="seen-status"></i></span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        return htmlText;
    };

    this.completeRequest = function() {
        if (!this.request.currentRequest.conclusionExists) {
            //        bootbox.alert("Добавьте заключение");
            PopupError('Добавьте заключение');

            $('#showConclusions').trigger('click');
            return false;
        }

        if (this.request.currentRequest.callActive) {
            this.sendDrop();
            this.localDrop();
        }

        getData = {};
        getData.request_id = this.request.currentRequest.requestId;
        $.get('/mobile/doctor/complete_request', getData, function (data) {}, 'json');

        CM.consultationWindow.modal('hide');
        this.callUI.consultationWindow = false;
        this.emptyCallWindow();

        return false;
    };

    this.addNotificationBlock = function(notif) {
        readStatusClass = '';
        openingAction = 'onclick="CM.markNotificationAsRead('+ notif.id +', ' + (typeof notif.info.request_id !== 'undefined' ? notif.info.request_id : notif.object_id ) + ');"';
        if (notif.read == 0) {
            readStatusClass = 'new';
        }

        notifHTML = '';
        switch (notif.type) {
            case 'service_send_message' :
            //    incrementUnreadHistory(notif.object_id, 1);
                notifHTML = '' +
                    '<div id="notification' + notif.id + '" class="notif ' + readStatusClass + '" '+ openingAction +'>' +
                        '<div class="img_cont">' +
                            '<img src="/images/notification_default_icon.png">' +
                        '</div>' +
                        '<span class="notifDescr">' + notif.text + '<span class="time">' + getLiteralDate(notif.created) + '</span></span>' +
                    '</div>';
                break;
            default:
            //    incrementUnreadHistory(notif.object_id, 1);
                notifHTML = '' +
                    '<div id="notification' + notif.id + '" class="notif ' + readStatusClass + '" '+ openingAction +'>' +
                        '<div class="img_cont">' +
                            '<img src="/images/notification_default_icon.png">' +
                        '</div>' +
                        '<span class="notifDescr">' + notif.text + '<span class="time">' + getLiteralDate(notif.created) + '</span></span>' +
                    '</div>';
        }
        return notifHTML;
    };

    this.loadNotifications = function(start, count) {
        updatingNotifications = true;
        if (typeof start === 'undefined') start = 0;
        if (typeof count === 'undefined') count = 5;
        $('.notifs_cont').addClass('loading');
        $('.loader').addClass('visible');
        $.post('/mobile/notifications/get_service_notifications/?start='+start+'&count='+count, [], function(data) {
            $('.notifs_cont').removeClass('loading');
            if (data.notifications.length > 0) {

                $('#notification_count').remove();
                for (var i= 0; i<data.notifications.length; i++) {
                    notif = data.notifications[i];

                    notifHTML = CM.addNotificationBlock(notif);

                    $('.notifs_cont').find('.loader').remove();
                    $('.notifs_cont').append(notifHTML);
                    $('.notifs_cont').append('<div class="loader"><img class="loading visible" src="/images/loading.gif"></div>');
                }
                $('#notifs .ico').removeClass('opening');
                if ($('.notifs_cont .notif').length > 0) {
                    $(".no-notifs").remove();

                    $(".notifs_cont").perfectScrollbar('update');
                }
            } else {

                $('.loader').removeClass('visible');
               // $('.notifs_cont').scrollTop($('.notifs_cont').prop('scrollHeight')-410);
                $('.notifs_cont').scrollTop($('.notifs_cont').prop('scrollHeight') - $('.notifs_cont').height() - 10);
                $(".notifs_cont").perfectScrollbar('update');
                $('.no-notifs').addClass('visible');
            }
            updatingNotifications = false;
        });
    };

    this.markNotificationAsRead = function(notificationId, requestId) {

        $.ajax({
            url: '/mobile/doctor/read_notification/',
            data: {id: notificationId, request_id: requestId},
            success: function(data) {
                $('#notifs').find('#notification' + notificationId).removeClass('new');
                CM.openRequestFromNotifications(requestId, data.request_state);
            }
        });

    };

    this.openRequestFromNotifications = function(requestId, requestState) {
        switch (requestState) {
            case 'initiated':
                document.location.href = '/doctor/chosen/?request_id=' + requestId;
                break;
            case 'serving':
                document.location.href = '/doctor/serving/?request_id=' + requestId;
                break;
            default:
                PopupError('Невозможно открыть, консультация закрыта.');
        }
    };

    this.getMedCard = function() {
        if (CM.request.currentRequest.medcardLoaded) return;
        $('#medcard-container').addClass('loading').find('.medcard').empty();
        $.ajax({
            url: '/mobile/doctor/get_user_conclusions/',
            type: 'GET',
            dataType: 'json',
            data: {user_id: CM.request.currentRequest.userId},
            success: function(data) {
                $('#medcard-container').removeClass('loading');
                if (data.status != "OK") {
                    console.log(data.errors);
                } else {
                    CM.request.currentRequest.medcardLoaded = true;
                    if (data.requests.length > 0) {
                        for (i = 0; i < data.requests.length; i++) {
                            request = data.requests[i];
                            request.doctorName = request.first_name + ' ' +request.middle_name + ' ' +request.last_name + ' ';
                            switch (request.status) {
                                case 'initiated':
                                    statusClass = 'initiated';
                                    status = 'обращение создано';
                                    dateInfo = '<span class="info-text">соадано: </span><span class="date">'+getLiteralDate(request.creation_time)+'</span>';
                                    break;
                                case 'serving':
                                    statusClass = 'serving';
                                    status = 'в процессе';
                                    dateInfo = '<span class="info-text">начало: </span><span class="date">'+getLiteralDate(request.answer_time)+'</span>';
                                    break;
                                case 'solved':
                                    statusClass = 'solved';
                                    status = 'обращение закрыто';
                                    dateInfo = '<span class="info-text">завершение: </span><span class="date">'+getLiteralDate(request.close_time)+'</span>';
                                    break;
                                case 'canceled_by_user':
                                    statusClass = 'canceled';
                                    status = 'обращение отменено';
                                    dateInfo = '<span class="info-text">отмена: </span><span class="date">'+getLiteralDate(request.cancellation_time)+'</span>';
                                    break;
                                case 'canceled_by_doctor':
                                    statusClass = 'canceled';
                                    status = 'обращение отменено';
                                    dateInfo = '<span class="info-text">отмена: </span><span class="date">'+getLiteralDate(request.cancellation_time)+'</span>';
                                    break;
                                default :
                                    statusClass = '';
                                    status = '';
                                    dateInfo = '';
                            }
                            if (i == 0) {
                                requestHTML = '';
                            } else {
                                requestHTML = '<hr>';
                            }
                            requestHTML += '' +
                                '<div class="medcard-request '+statusClass+'" id="medCardRequest'+request.id+'">' +
                                    '<div class="request-header">' +
                                        '<div class="avatar-cont">' +
                                            '<img src="/mobile/user/userpic/'+request.doctor_id+'/thumb/">' +
                                        '</div>' +
                                        '<div class="name-wrapper">' +
                                            '<span class="name">'+request.doctorName+'</span>' +
                                            '<span class="status">'+status+'</span>' +
                                        '</div>' +
                                        '<div class="date-wrapper">' +
                                            dateInfo +
                                            //'<span class="date-start">'+getLiteralDate(request.creation_time)+'</span>' +
                                            //'<span class="date-end">'+(request.close_time == '0000-00-00 00:00:00' ? 'в процессе' : getLiteralDate(request.close_time))+'</span>' +
                                        '</div>' +
                                        '<div class="buttons-wrapper">' +
                                            '<span class="toggle-conclusions-state blue-button-empty" onclick="CM.toggleConclusionsState(this);">Заключения</span>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="clearFloat"></div>' +
                                    '<div class="request-conclusions-wrapper">';

                            if (request.conclusions.length > 0) {
                                for (j = 0; j < request.conclusions.length; j++) {
                                    requestHTML += CM.appendConclusionToMedCard(request.conclusions[j]);
                                }
                            } else {
                                requestHTML += '<span class="no-conclusions">Нет заключений.</span>'
                            }

                            requestHTML += '' +
                                    '</div>' +
                                '</div>';

                            $('#medcard-container .medcard').append(requestHTML);

                        }

                    }
                }
            },
            error: function(e) {
                $('#medcard-container').removeClass('loading');
            }
        })
    };

    this.appendConclusionToMedCard = function(conclusion) {
        htmlText = '' +
            '<div class="textblock">' +
                '<div class="cont">' +
                    '<div class="conclusion-wrapper">' +
                        '<span class="conclusion-title">Заключение <span class="datetime"> - '+getLiteralDate(conclusion.creation_time)+'</span></span>' +
                        '<span class="conclusion-text">' + conclusion.conclusion + '</span>' +
                    '</div>' +
                '</div>' +
            '</div>';
        return htmlText;
    };

    this.toggleConclusionsState = function(element) {
        var medCardRequest = $(element).parents('.medcard-request');
        if (medCardRequest.hasClass('expanded')) {
            medCardRequest.removeClass('expanded')
                .find('.toggle-conclusions-state').text('Заключения');
        } else {
            medCardRequest.addClass('expanded')
                .find('.toggle-conclusions-state').text('Скрыть');
        }
    };

    this.deleteCertificate = function(id, filename) {
        $.ajax({
            url: '/mobile/doctor/delete_cert/?id='+id+'&name='+filename,
            type: "GET",
            success: function(data){
                if (data.status != 'OK') {
                    PopupError('Удаление не удалось.');
                    console.log(data.errors);
                } else {
                    $('#cert_' + data.name_deleted).fadeOut(500);
                }
            }
        })
    }

};

DoctorCallModel.prototype = new CallModel();
DoctorWSReceived.prototype = new WSReceived();

