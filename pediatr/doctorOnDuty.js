var OndutyWSReceived = function() {
    this.createOnDuty = function(event){
        if (CM.request.requests.length == 0) {
            appendingHTML = '<audio class="notification_sound" src="/images/notification.wav" width="1px" height="1px" autoplay onended="RemoveNotification()"></audio>';
            $(document.body).append(appendingHTML);
        }
        CM.incrementBadgeCount('on_duty_count', 1);
        CM.addRequestToTable(event['result']['request']);
    };
    this.serveRequest = function(event){
        for( var i = 0; i < CM.request.requests.length; i++) {
            if ( CM.request.requests[i].requestId == event['result']['request_id']) {
                CM.request.requests.splice(i, 1);
                CM.incrementBadgeCount('on_duty_count', -1);
                $('#paientsTable').dataTable().fnDeleteRow(i);
            }
        }
    };
};

OndutyWSReceived.prototype = new DoctorWSReceived();

var ondutyDoctorCallModel = function() {

    this.pageAction = 'onduty';

    this.wsReceived = new OndutyWSReceived();

    this.serveRequest = function() {
        if (!hasVideoDevice) {
            PopupError('Для консультирования пациентов вам необходимо наличие вэб камеры. Подсоедините камеру или обратитесь в нашу службу технической поддержки.');
            return false;
        }
        if (CM.request.requests[0] === null || typeof CM.request.requests[0] == 'undefined') {
            PopupError('В очереди нет пациентов');
            return;
        }
        CM.request.currentRequest = CM.request.requests[0];
        CM.openTok.requestType = CM.request.currentRequest.requestType;

        if (this.request.currentRequest.phone == '') {
            $('.send-call-sip').addClass('no-phone');
        }

        CM.consultationWindow.modal('show')
            .find('#consultationWindowHeader')
                .find('.avatar').attr('src', '/mobile/user/userpic/' + CM.request.currentRequest.userId + '/thumb/?' + new Date().getTime()+'').end()
                .find('.name').text(CM.request.currentRequest.userName).end()
            .end()
            .find('#videos').css('background-image', 'url("/mobile/user/userpic/'+CM.request.currentRequest.userId+'/?' + new Date().getTime()+'")');

        CM.showCallPopup(CM.request.currentRequest.userId);
        CM.sendStart();

    };

    this.addRequestToTable = function(request) {
        tableData = [];

        tableData[0] = request['user_first_name'];
        tableData[1] = '00:00:00';
        obj = new Request();
        obj.userId = request['user_id'];
        obj.phone  = request['user_phone'];
        obj.userName = request['user_first_name'];
        obj.requestId = request['id'];
        obj.waitTime = request['creation_time'];
        obj.requestType = 'video';
       // obj.requestType = request['request_type'];

        this.request.requests.push(obj);

        $('#paientsTable').dataTable().fnAddData(tableData);
    };

    this.sentStartStuff = function(result) {
        //appendingHTML = CM.appendCallStarted(getFormattedDateTime());
        //CM.addHTMLToChat(appendingHTML);
        for( var i = 0; i < this.request.requests.length; i++) {
            if ( this.request.requests[i].requestId == result['request_id']) {
                this.request.requests.splice(i, 1);
                CM.incrementBadgeCount('on_duty_count', -1);
                $('#paientsTable').dataTable().fnDeleteRow(i);
                break;
            }
        }
        this.request.currentRequest.unread = 1;
        this.request.currentRequest.conversationId = result['conversation_id'];
        $('.in-call-wrapper .send-call').addClass('drop-call');
        $('.outgoing-call-wrapper .drop-call').addClass('active');
    };

};

ondutyDoctorCallModel.prototype = new DoctorCallModel();

var CM = new ondutyDoctorCallModel();
CM.openTok.openTokApiKey = openTokApiKey;
var browser = CM.openTok.browser = CM.browser;

$(document).ready(function () {

    CM.consultationWindow = $('#consultationWindow');
    CM.myId = $('#myId').val();

    $.extend($.gritter.options, {
        position: 'bottom-left', // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
        fade_in_speed: 'medium', // how fast notifications fade in (string or int)
        fade_out_speed: 2000, // how fast the notices fade out
        time: 6000 // hang on the screen for...
    });

    CM.consultationWindow.click(function(){
        if (CM.request.currentRequest.unread > 0) {
            CM.markRequestAsRead(CM.request.currentRequest.requestId);
        }
    }).on('hidden.bs.modal', function () {
        CM.emptyCallWindow();
        onDropClick();
    });

    $("#callNext").click(function () {
        CM.serveRequest();
    });

    $("#logout").click(function () {
        $.get("/logout/", function (data) {
            loginForm();
        })
    });

    $('#paientsTable').dataTable({
        "bSort": false,
        "oLanguage": {
            "sZeroRecords":  "Ждем пациентов.",
            "sProcessing":   "Подождите...",
            "sInfo":         "Пациенты _START_ - _END_ из _TOTAL_",
            "sInfoEmpty":    "Пациентов нет",
            "oPaginate": {
                "sFirst": "Перв.",
                "sPrevious": "Пред.",
                "sNext": "След.",
                "sLast": "Посл."
            }
        },

        "oTableTools": {
            "sSwfPath": "/js/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 10,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [ "no-sort" ]
            }
        ]
    });

    $.ajax({
        url: '/mobile/doctor/get_on_duty_requests/',
        data: {},
        dataType: 'json',
        async: false,
        success: function(data) {
            if (data.status == 'error') {
                console.log('Errors on loading on duty requests');
            } else {
                forEach(data.requests, function (key, value) {
                    CM.addRequestToTable(value);
                })
            }

            setInterval(function() { CM.incrementWaitingTime(); }, 1000);
        }
    });

    $('.send-button').click(function(){
        if (!$(this).hasClass('disabled')) {
            $(this).parent().submit();
        }
    });

    $('.conclusion-form').submit(function(e){
        e.preventDefault();
        CM.addConclusion($(this).find('textarea').val());
    });

    $('#addConclusion').click(function(e){
        e.preventDefault();
        $('.conclusion-form').submit();
    });

    $('.messaging-form').submit(function(e){
        e.preventDefault();
        CM.addMessage($(this).find('.input-msg').val());
        $(this).find('.input-msg').val('');
    });

    $('.doctor-actions .action').click(function(e){
        if ($(this).data('value') === null || $(this).hasClass('disabled') ||  $($(this).data("value")).length == 0) {
            return;
        }
        if ( !$(this).hasClass("active") ) {
            $('.doctor-actions').find('.active').removeClass("active");
            $('.chat-wrapper').addClass('inactive');
            $(this).addClass("active");
            $($(this).data("value")).removeClass('inactive');
        }
    });

    $('#postponeRequest').click(function(e){
        extendRequest();
    });

    $('#closeRequest').click(function(e){
        completeRequest();
    });

    $('.options-button').click(function(e){
        e.preventDefault();
        ToggleOptionsState();
    });

    $('.send-call').click(function(e){
        if ($(this).hasClass('disabled')) return;
        if (!CM.request.currentRequest.callActive === true) {
            onCallClick();
        } else {
            onDropClick();
        }
    });

    $('.send-call-sip').click(function(e){
        if ($(this).hasClass('disabled')) return;
        if ($(this).hasClass('in-call')) {
            CM.dropSip();
        } else {
            CM.callSip();
        }
    });

    $('.share-video').click(function(e){
        if ($(this).hasClass('not-active')) return;
        toggleVideo();
    });

    $('.share-audio').click(function(e){
        if ($(this).hasClass('not-active')) return;
        toggleAudio();
    });

    CM.consultationWindow.find('.options-menu-wrapper li').click(function(e){
        e.preventDefault();
        var step = $(this).data('step');
        CM.consultationWindow.find('.options-menu-wrapper li').each(function(){
            if ($(this).data('step') != step) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        });
        CM.consultationWindow.find('.testing-steps li').each(function(){
            if ($(this).data('step') != step) {
                $(this).removeClass('active').removeClass('testing');
                if ($(this).data('step') == 'camera') {
                    testManager.clearCamera();
                } else if ($(this).data('step') == 'microphone') {
                    testManager.clearMicrophone();
                }
            } else {
                $(this).addClass('active');
            }
        });
    });

    WebSocketLocal.CommandResultReceived = function (result) {
        if (result['result']['status'] != 'OK') {
            console.log(result);
            return;
        }

        switch (result['name']) {
            case '/mobile/user/online_status/':
                break;
            case '/mobile/user/online_status/get/':
                break;
            case '/mobile/user/online_status/update/':
                break;
            case '/mobile/conversation/start/':
                break;
            case '/mobile/conversation/take/':
                break;
            case '/mobile/conversation/drop/':
                break;
            case '/mobile/doctor/add_conclusion/':
                break;
            case '/mobile/conversation/send_message/':
                break;
            default:
        }
    };

    WebSocketLocal.EventReceived = function (event) {
        console.log(event);

        switch (event['name']) {
            case '/mobile/user/online_status/update/':
                break;
            case '/mobile/conversation/start/':

            case'/mobile/doctor/serve_request/':
                CM.wsReceived.serveRequest(event);
                break;
            case '/mobile/user/create_on_duty_request/':
                CM.wsReceived.createOnDuty(event);
                break;
            case '/mobile/user/create_chosen_request/':
                CM.wsReceived.createChosen(event);
                break;
            case '/mobile/conversation/send_notification/':
                CM.wsReceived.sendNotification(event);
                break;
            case '/mobile/user/user_online/':
                CM.wsReceived.userOnline(event);
                break;
            case '/mobile/conversation/take/':
                CM.wsReceived.takeCall(event);
                break;
            case '/mobile/user/answer_as_chat/':
                CM.wsReceived.answerAsChat(event);
                break;
            case "/mobile/user/add_request_attachment/":
                CM.wsReceived.addAttachment(event);
                break;
            case '/mobile/conversation/send_message/':
                CM.wsReceived.sendMessage(event);
                break;
            case '/mobile/user/mark_as_read/' :
                CM.wsReceived.markAsRead(event);
                break;
            case '/mobile/conversation/drop/':
                CM.wsReceived.dropCall(event);
                break;
            case '/mobile/doctor/5_minutes_left/':
                CM.wsReceived.fiveMinutesLeft(event);
                break;
            case '/mobile/user/start_typing/':
                CM.wsReceived.startTyping(event);
                break;
            case '/mobile/user/stop_typing/':
                CM.wsReceived.stopTyping(event);
                break;
        }
    };

    $("#online_status_combo a").click(function () {
        var arguments = {};
        arguments['online_status'] = $(this).html().toLowerCase();
        WebSocketLocal.RequestCommand('/mobile/user/online_status/update/', arguments);
        return true;
    });

});


function dropBeforeTake() {
    CM.dropBeforeTake();
}
function CallSipClicked(){
    if ($('#callSip').hasClass('in-call')) {
        CM.dropSip();
    } else {
        CM.callSip();
    }
}
function onCallClick() {
    CM.onCallClick();
}
function onDropClick() {
    CM.onDropClick();
}
function toggleVideo() {
    var shareButton = $('.share-video');
    if (CM.openTok.publishVideo) {
        shareButton.addClass('disabled');
    } else {
        $('.video-wrapper').addClass('visible');
        $('.chat-wrapper').removeClass('wide');
        shareButton.removeClass('disabled');
    }
    updateScrollbar($('#chat-container'));
    CM.openTok.toggleVideo();
}
function toggleAudio() {
    var shareButton = $('.share-audio');
    if (CM.openTok.publishAudio) {
        shareButton.addClass('disabled');
    } else {
        shareButton.removeClass('disabled');
    }
    CM.openTok.toggleAudio();
}
function extendRequest() {
    CM.extendRequest();
}
function completeRequest() {
    CM.completeRequest();
}
function parentInfo() {
    CM.parentInfo();
}
function getConclusions() {
    CM.getConclusions();
}
function getMedCard() {
    CM.getMedCard();
}
function RemoveNotification() {
    $('.notification_sound').remove()
}
function ToggleFullScreen() {
    CM.openTok.toggleFullScreen();
}
function ShowConclusionPopup() {
    CM.showConclusionPopup();
}
function HideConclusionPopup() {
    CM.hideConclusionPopup();
}
function ToggleOptionsState() {
    CM.toggleOptionsState();
}
