var ServingWSReceived = function() {

};

ServingWSReceived.prototype = new DoctorWSReceived();

var servingDoctorCallModel = function() {

    this.pageAction = 'serving';

    this.wsReceived = new ServingWSReceived();

    this.openRequest = function(i) {
        if (this.request.requests[i] === null || typeof this.request.requests[i] == 'undefined') {
            PopupError('Заявка не найдена.');
            return;
        }

        this.request.currentRequest = this.request.requests[i];
        this.openTok.requestType = this.request.currentRequest.requestType;

        this.request.currentRequest.tableIndex = i;

        this.getChat();

        if (this.request.currentRequest.phone == '') {
            $('.send-call-sip').addClass('no-phone');
        }

        CM.consultationWindow.modal('show')
            .find('#consultationWindowHeader')
                .find('.avatar').attr('src', '/mobile/user/userpic/' + CM.request.currentRequest.userId + '/thumb/?' + new Date().getTime()+'').end()
                .find('.name').text(CM.request.currentRequest.userName).end()
            .end()
            .find('#videos').css('background-image', 'url("/mobile/user/userpic/'+CM.request.currentRequest.userId+'/?' + new Date().getTime()+'")');

        this.callUI.consultationWindow = true;
        $('.input-msg').focus();
    };

    this.openRequestFromNotifications = function(requestId, requestState){
        canOpenRequest = false;

        for (i=0; i<this.request.requests.length; i++) {
            if (this.request.requests[i].requestId == requestId) {
                canOpenRequest = true;
            }
        }

        if (!canOpenRequest) {
            switch (requestState) {
                case 'initiated':
                    document.location.href = '/doctor/initiated/?request_id=' + requestId;
                    break;
                case 'serving':
                    PopupError('Невозможно открыть консультацию. Обновите страницу.');
                    break;
                default:
                    PopupError('Невозможно открыть, консультация закрыта.');
            }
        } else {
            CM.openRequest(requestId);
        }
    };

    this.addRequestToTable = function(request, i) {
        tableData = [];
        switch (request['consultation_type'])
        {
            case 'chosen':
                tableData[0]='<div class="chosen"></div> ';
                break;
            default :
                tableData[0]='<div class="on_duty"></div>';
                break;
        }

        if(request['online_status']=='online') {
			tableData[1]='<div class="onlineStatus online"></div>';
        } else {
			tableData[1]='<div class="onlineStatus offline"></div>';
		}

        cnt = (request['unread_message_count']>0 ? request['unread_message_count'] : '');
        tableData[2] = '<div class="user-name">'+request['user_first_name']+'<span class="badge-wrapper"><span class="badge request-unread-count" data-id="'+request['id']+'">'+cnt+'</span></span><div>';

        tableData[3]=request['finished'];
        tableData[4]=request['missed'];

        tableData[5] = request['creation_time'];
        date = new Date(tableData[3]);
        tableData[6]="<div class='blue-button' onclick='openRequest("+i+")'>Открыть</div>";

        replaced = request['creation_time'];
        if (replaced) {
            replaced = replaced.replace('-', '/');
            replaced = replaced.replace('-', '/')
        }
        dateObj = new Date(replaced);

        obj = new Request();
        obj.profileId = request['profile_id'];
        obj.userId = request['user_id'];
        obj.userName = request['user_first_name'];
        obj.phone  = request['user_phone'];
        obj.requestOpened = request[''];
        obj.requestId = request['id'];
        obj.requestType = 'video';
      //  obj.requestType = request['request_type'];
        obj.unread = request['unread_message_count'];
        obj.waitTime = ~~((Date.now() - dateObj.getTime()) / 1000);

        this.request.requests.push(obj);

        $('#paientsTable').dataTable().fnAddData(tableData);
        if(request.consultation_type=='chosen'){
            $('#paientsTable').dataTable().api().row($('#paientsTable').dataTable().api().row()[0].length-1).node().style.background='#EBEBEB'
        } else {
            $('#paientsTable').dataTable().api().row($('#paientsTable').dataTable().api().row()[0].length-1).node().style.background='#ffffff'
        }


    };
	
    this.sentStartStuff = function(result) {
        //appendingHTML = CM.appendCallStarted(getFormattedDateTime());
        //CM.addHTMLToChat(appendingHTML);
        this.request.currentRequest.conversationId = result['conversation_id'];
        $('.in-call-wrapper .send-call').addClass('drop-call');
        $('.outgoing-call-wrapper .drop-call').addClass('active');
    };

};

servingDoctorCallModel.prototype = new DoctorCallModel();

var CM = new servingDoctorCallModel();
CM.openTok.openTokApiKey = openTokApiKey;
var browser = CM.openTok.browser = CM.browser;

var version;

$(document).ready(function() {

    CM.consultationWindow = $('#consultationWindow');
    CM.myId = $('#myId').val();

    $.extend($.gritter.options, {
        position: 'bottom-left', // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
        fade_in_speed: 'medium', // how fast notifications fade in (string or int)
        fade_out_speed: 2000, // how fast the notices fade out
        time: 6000 // hang on the screen for...
    });

    CM.consultationWindow.click(function(){
        if (CM.request.currentRequest.unread > 0) {
            CM.markRequestAsRead(CM.request.currentRequest.requestId);
        }
    }).on('hidden.bs.modal', function () {
        CM.emptyCallWindow();
        onDropClick();
    });

    $('#paientsTable').dataTable({
        "bSort": false,
        "oLanguage": {
            "sZeroRecords":  "Ждем пациентов.",
            "sProcessing":   "Подождите...",
            "sInfo":         "Пациенты _START_ - _END_ из _TOTAL_",
            "sInfoEmpty":    "Пациентов нет",
            "oPaginate": {
                "sFirst": "Перв.",
                "sPrevious": "Пред.",
                "sNext": "След.",
                "sLast": "Посл."
            }
        },

        "oTableTools": {
            "sSwfPath": "/js/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 10,
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [ "no-sort" ]
            }
        ]
    });

    $.ajax({
        url: '/mobile/doctor/get_requests/?status_arr=serving',
        data: {},
        dataType: 'json',
        async: false,
        success: function(data) {
            if (data.status == 'error') {
                console.log('Errors on loading on duty requests');
            } else {
                $.ajax({
                    url: '/mobile/doctor/online_users/',
                    data: {},
                    dataType: 'json',
                    async: false,
                    success: function(user) {
                        if (data.status == 'error') {
                            console.log('Errors on loading on duty requests');
                        } else {

                            i=0;
                            forEach(data.requests, function(key, value) {

                                value['online_status']='offline';
                                forEach(user.users, function(key2, value2){
                                    arr[value2['user_id']]=value2['user_id'];
                                    if(value2['user_id']==value['user_id'])
                                    {
                                        value['online_status']='online'
                                    }
                                });
                                CM.addRequestToTable(value, i);
                                i++;
                            })
                        }
                        $('#serving_count').text((countProperties(arr)>0?countProperties(arr):''));
                    }
                });
            }
        }
    });

	$("#logout").click(function () {
        $.get("/logout/", function (data) {
            loginForm();
        })
    });

    $('.send-button').click(function(){
        if (!$(this).hasClass('disabled')) {
            $(this).parent().submit();
        }
    });

    $('.conclusion-form').submit(function(e){
        e.preventDefault();
        CM.addConclusion($(this).find('textarea').val());
    });

    $('#addConclusion').click(function(e){
        e.preventDefault();
        $('.conclusion-form').submit();
    });

    $('.messaging-form').submit(function(e){
        e.preventDefault();
        CM.addMessage($(this).find('.input-msg').val());
        $(this).find('.input-msg').val('');
    });

    $('.doctor-actions .action').click(function(e){
        if ($(this).data('value') === null || $(this).hasClass('disabled') ||  $($(this).data("value")).length == 0) {
            return;
        }
        if ( !$(this).hasClass("active") ) {
            $('.doctor-actions').find('.active').removeClass("active");
            $('.chat-wrapper').addClass('inactive');
            $(this).addClass("active");
            $($(this).data("value")).removeClass('inactive');
        }
    });

    $('#postponeRequest').click(function(e){
        extendRequest();
    });

    $('#closeRequest').click(function(e){
        completeRequest();
    });

    $('.options-button').click(function(e){
        e.preventDefault();
        ToggleOptionsState();
    });

    $('.send-call').click(function(e){
        if ($(this).hasClass('disabled')) return;
        if (!CM.request.currentRequest.callActive === true) {
            onCallClick();
        } else {
            onDropClick();
        }
    });

    $('.send-call-sip').click(function(e){
        if ($(this).hasClass('disabled')) return;
        if ($(this).hasClass('in-call')) {
            CM.dropSip();
        } else {
            CM.callSip();
        }
    });

    $('.share-video').click(function(e){
        if ($(this).hasClass('not-active')) return;
        toggleVideo();
    });

    $('.share-audio').click(function(e){
        if ($(this).hasClass('not-active')) return;
        toggleAudio();
    });

    CM.consultationWindow.find('.options-menu-wrapper li').click(function(e){
        e.preventDefault();
        var step = $(this).data('step');
        CM.consultationWindow.find('.options-menu-wrapper li').each(function(){
            if ($(this).data('step') != step) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        });
        CM.consultationWindow.find('.testing-steps li').each(function(){
            if ($(this).data('step') != step) {
                $(this).removeClass('active').removeClass('testing');
                if ($(this).data('step') == 'camera') {
                    testManager.clearCamera();
                } else if ($(this).data('step') == 'microphone') {
                    testManager.clearMicrophone();
                }
            } else {
                $(this).addClass('active');
            }
        });
    });

    WebSocketLocal.CommandResultReceived = function (result) {
        if (result['result']['status'] != 'OK') {
            console.log(result);
            return;
        }

        switch (result['name']) {
            case '/mobile/user/online_status/':
                break;
            case '/mobile/user/online_status/get/':
                break;
            case '/mobile/user/online_status/update/':
                break;
            case '/mobile/conversation/start/':
                break;
            case '/mobile/conversation/take/':
                break;
            case '/mobile/conversation/drop/':
                break;
            case '/mobile/doctor/add_conclusion/':
                break;
            case '/mobile/conversation/send_message/':
                break;
            default:
        }
    };

    WebSocketLocal.EventReceived = function (event) {
        console.log(event);

        switch (event['name']) {
            case '/mobile/user/online_status/update/':
                break;
            case '/mobile/conversation/start/':
                CM.wsReceived.startCall(event);
                break;
            case '/mobile/user/create_on_duty_request/':
                CM.wsReceived.createOnDuty(event);
                break;
            case '/mobile/user/create_chosen_request/':
                CM.wsReceived.createChosen(event);
                break;
            case '/mobile/conversation/send_notification/':
                CM.wsReceived.sendNotification(event);
                break;
            case '/mobile/user/user_online/':
                CM.wsReceived.userOnline(event);
                break;
            case '/mobile/conversation/take/':
                CM.wsReceived.takeCall(event);
                break;
            case '/mobile/user/answer_as_chat/':
                CM.wsReceived.answerAsChat(event);
                break;
            case "/mobile/user/add_request_attachment/":
                CM.wsReceived.addAttachment(event);
                break;
            case '/mobile/conversation/send_message/':
                CM.wsReceived.sendMessage(event);
                break;
            case '/mobile/user/mark_as_read/' :
                CM.wsReceived.markAsRead(event);
                break;
            case '/mobile/conversation/drop/':
                CM.wsReceived.dropCall(event);
                break;
            case '/mobile/doctor/5_minutes_left/':
                CM.wsReceived.fiveMinutesLeft(event);
                break;
            case '/mobile/user/start_typing/':
                CM.wsReceived.startTyping(event);
                break;
            case '/mobile/user/stop_typing/':
                CM.wsReceived.stopTyping(event);
                break;
        }
    };

    $("#online_status_combo a").click(function () {
        var arguments = {};
        arguments['online_status'] = $(this).html().toLowerCase();
        WebSocketLocal.RequestCommand('/mobile/user/online_status/update/', arguments);
        return true;
    });

});

function dropBeforeTake() {
    CM.dropBeforeTake();
}
function CallSipClicked(){
    var sipButton = $('#callSip');
    if (sipButton.hasClass('disabled')) return;
    if (sipButton.hasClass('sending')) return;

    if (sipButton.hasClass('in-call')) {
        CM.dropSip();
    } else {
        CM.callSip();
    }
}
function onCallClick() {
    CM.onCallClick();
}
function onDropClick() {
    CM.onDropClick();
}
function toggleVideo() {
    var shareButton = $('.share-video');
    if (CM.openTok.publishVideo) {
        shareButton.addClass('disabled');
    } else {
        $('.video-wrapper').addClass('visible');
        $('.chat-wrapper').removeClass('wide');
        shareButton.removeClass('disabled');
    }
    updateScrollbar($('#chat-container'));
    CM.openTok.toggleVideo();
}
function toggleAudio() {
    var shareButton = $('.share-audio');
    if (CM.openTok.publishAudio) {
        shareButton.addClass('disabled');
    } else {
        shareButton.removeClass('disabled');
    }
    CM.openTok.toggleAudio();
}
function extendRequest() {
    CM.extendRequest();
}
function completeRequest() {
    CM.completeRequest();
}
function parentInfo() {
    CM.parentInfo();
}
function getConclusions() {
    CM.getConclusions();
}
function getMedCard() {
    CM.getMedCard();
}
function openRequest(element) {
    CM.openRequest(element);
}
function RemoveNotification(){
    $('.notification_sound').remove();
}
function ToggleFullScreen(){
    CM.openTok.toggleFullScreen();
}
function ShowConclusionPopup() {
    CM.showConclusionPopup();
}
function HideConclusionPopup() {
    CM.hideConclusionPopup();
}
function Archive(){
    url = 'https://api.opentok.com/v2/partner/' + CM.openTok.openTokApiKey + '/archive';
    PARTNER = CM.openTok.openTokApiKey + ':2a227add0bb316a14741e95d40a164ab7ab42001';
    var data = [];
    data['sessionId'] = CM.openTok.sessionId;
    data['name'] = 'barev';
    $.ajax({
        crossDomain: true,
        type: 'POST',
        url: url,
        headers: {
            "X-TB-PARTNER-AUTH":PARTNER,
            "Content-Type":'application/json',
            "Access-Control-Allow-Origin":'*'
        },
        //OR
        beforeSend: function(xhr) {
          xhr.setRequestHeader("X-TB-PARTNER-AUTH", 'PARTNER');
          xhr.setRequestHeader("Content-Type", "application/json");
          xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
        }
    }).done(function(data) {
        console.log(data);
    });
}

function ToggleOptionsState() {
    CM.toggleOptionsState();
}
