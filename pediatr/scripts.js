/**
 * Created by Ticco on 12/6/2014.
 */
function payForReserve(doctor_id)
{
    $.ajax({
            url: '/mobile/user/get_uniteller_links/?type=chosen&doctor_id='+doctor_id,
            success:    function(e){
                if(e.status=='OK'){
                    $('#uniteller_form input[name="Shop_IDP"]').val(e.data.shop_id)
                    $('#uniteller_form input[name="Order_IDP"]').val(e.data.order_id)
                    $('#uniteller_form input[name="Subtotal_P"]').val(e.data.price)
                    $('#uniteller_form input[name="Signature"]').val(e.data.signature)
                    $('#uniteller_form input[name="Phone"]').val(e.data.phone)
                    $('#uniteller_form input[name="Email"]').val(e.data.email)
                    $('#uniteller_form').submit();

                }
            }
        }
    )
}
function refillBalance(ammount)
{
    $.ajax({
        url: '/mobile/user/get_uniteller_links/?type=refill&ammount='+ammount ,
        success:    function(e){
            if(e.status=='OK'){
                $('#uniteller_form input[name="Shop_IDP"]').val(e.data.shop_id)
                $('#uniteller_form input[name="Order_IDP"]').val(e.data.order_id)
                $('#uniteller_form input[name="Subtotal_P"]').val(e.data.price)
                $('#uniteller_form input[name="Signature"]').val(e.data.signature)
                $('#uniteller_form input[name="Phone"]').val(e.data.phone)
                $('#uniteller_form input[name="Email"]').val(e.data.email)
                $('#uniteller_form').submit();

            }
        }
    })
}
var CM = new UserCallModel();
var version;

//WebSockets Object
WebSocketLocal.Host = webSocketHost;
var browser = CM.openTok.browser = CM.browser;
WebSocketLocal.SessionId = GetCookie('PHPSESSID');

function clearAttachmentForm() {
    $('.attachmentFile').val('');
    $('#selectAttachment').val('Выбрать файл');
    $('.description').val('');
}

function onDropClick() {
    CM.guid = true;
    CM.onDropClick();
}

function toggleVideo() {
    var shareButton = $('.share-video');
    if (CM.openTok.publishVideo) {
        shareButton.addClass('disabled');
    } else {
        $('.video-wrapper').addClass('visible');
        $('.chat-wrapper').removeClass('wide');
        shareButton.removeClass('disabled');
    }
    updateScrollbar($('#chat-container'));
    CM.openTok.toggleVideo();
}

function toggleAudio() {
    var shareButton = $('.share-audio');
    if (CM.openTok.publishAudio) {
        shareButton.addClass('disabled');
    } else {
        shareButton.removeClass('disabled');
    }
    CM.openTok.toggleAudio();
}

function OpenChatFromHistory(element) {
    requestWraper = $(element).parents('.history-item-wrapper');
    SetRequestActiveFromHistory(requestWraper);
    CM.openChatFromHistory();
}

function SetRequestActiveFromHistory(requestWrapper) {
    CM.request.currentRequest = {
        requestId: requestWrapper.find('.request-id').val(),
        callActive: false,
        doctorId: requestWrapper.find('.doctor-id').val(),
        doctorName: requestWrapper.find('.name').text(),
        doctorProfession: requestWrapper.find('.doctor-profession').val(),
        unread: requestWrapper.find('.request-unread-count').text(),
        requestType: requestWrapper.find('.request-type').val(),
        resolutionNeeded: requestWrapper.find('.resolution-needed').val()
    };
}

function RemoveNotification(){
    $('.notification_sound').remove()
}

function ToggleFullScreen(){
    CM.openTok.toggleFullScreen();
}

function attachFile(){
    $('#attachModal').modal('show');
}

function ResendPopupConfirmationCode() {
    $('#resendCodePopupSubmit').addClass('sending');
    $.ajax({
        url: '/mobile/user/resend_sms/',
        data: {},
        error: function(){
            PopupError('Ошибка связи');
            $('#resendCodePopupSubmit').removeClass('sending');
        },
        success: function(data) {
            $('#resendCodePopupSubmit').removeClass('sending');
            if (data['status'] != 'OK') {
                switch (data['errors']) {
                    case 'phone_already_confirmed':
                        PopupError('Номер уже подтвержден');
                        break;
                    case 'invalid_user_id':
                        PopupError('Пользователь не найден');
                        break;
                    default :
                        //TODO: create all possible cases with their error-alerting popups...
                        console.log(data['errors']);
                }
            } else {
                PopupSuccess('Новый код подтверждения успешно выслан');
            }
        }
    })
}

function ToggleOptionsState() {
    CM.toggleOptionsState();
}

function ShowRatingPopup(requestId, doctor) {
    ratigPopup = $('#rateRequest');
    ratigPopup.modal('show').find('.rate-request-id').val(requestId);
    ratigPopup.find('.rate-request-doctor-image').attr('src', '/mobile/user/userpic/'+doctor.id+'/thumb/');
    ratigPopup.find('.rate-request-doctor').text(NameShortener(doctor.last_name + ' ' + doctor.first_name + ' ' + doctor.middle_name));
}

function OpenOrderModal() {
    if ($('#confirmCreation').length > 0) {
        $('#confirmCreation').modal('show');
    } else {
        document.location.href='/dashboard/#confirmCreation'
    }
}

function CancelActiveRequest() {
    $.ajax({
        url: "/mobile/user/cancel_request/?request_id=" + CM.request.currentRequest.requestId,
        success: function(data){
            if (data.status != 'OK') {
                switch (data['errors']) {
                    case 'request already is being served or solved':
                        PopupError('Невозможно отменить консультацию. Она либо уже отменена, либо она уже началась.');
                        break;
                    case 'too_late_to_cancel':
                        PopupError('Невозможно отменить консультацию, до ее начала осталось менее 24 часов.');
                        break;
                    default :
                        //TODO: create all possible cases with their error-alerting popups...
                        console.log(data['errors']);
                        PopupError('Невозможно отменить консультацию.');
                }
            } else {
                PopupSuccess('Консультация отменена.');
                updateScrollbar($('#chat-container'));
            }
        }
    });
}

function PostponeActiveRequest() {
    PreparePostponeModals();
    $('#postponeModal').modal('show');
}

function PreparePostponeModals() {
    var postponeCalendar = $('#postponeModal');
    var d = new Date();
    var curr_date = d.getDate();
    if (curr_date<10) curr_date = '0' + curr_date;
    var curr_month = d.getMonth()+1;
    if (curr_month<10) curr_month = '0' + curr_month;

    postponeCalendar.find('.dayDetailed .date').text(curr_date + ' ' + monthNamesAlt[curr_month-1]);
    SetPostponeCalendar(CM.request.currentRequest.doctorId);

    var confirmPopup = $('#confirmRequestPostponeModal');
    confirmPopup.find('.avatar-cont img').attr('src', '/mobile/user/userpic/'+CM.request.currentRequest.doctorId+'/thumb/');
    confirmPopup.find('.info-cont .name').text(NameShortener(CM.request.currentRequest.doctorName));
    confirmPopup.find('.info-cont .profession').text(CM.request.currentRequest.doctorProfession);

    postponeCalendar.modal('show');

}

$(document).ready(function() {

    $('.message').linkify();

    CM.consultationWindow = $('#consultationWindow');
    CM.myId = $('#myId').val();

    switch (window.location.hash) {
        case '#notActiveUser':
            $('#activateProfileFormModal').modal('show');
            break;
        case '#confirmCreation':
            $('#confirmCreation').modal('show');
            break;
        default:
    }

    var hasVideoDevice = false;

    if ("undefined" == typeof MediaStreamTrack || !MediaStreamTrack) {
        version=1;
    } else {
        version=2;
        if (typeof MediaStreamTrack.getSources !== 'undefined') {
            MediaStreamTrack.getSources(function (media_sources) {
                for (var i = 0; i < media_sources.length; i++) {
                    if (media_sources[i].kind == 'video') {
                        hasVideoDevice = true;
                    }
                }
            });
        }
    }

    initializeWebSockets();
    if ($('#testConnection').length == 0 && !testManager.testModuleResults.ws) {
        TestConnection();
    } else {
        SetTestingWSState(false);
    }

    $("#attachmentForm").submit(function(e){
        e.preventDefault();

        $('.attach-file').addClass('sending');

        $.ajax({
            url: '/mobile/user/add_request_attachment/',
            type: 'POST',
            data: new FormData($(this)[0]),
            success: function (data) {
                clearAttachmentForm();
                $('.attach-file').removeClass('sending');
                if (data['status'] != 'OK') {
                    switch (data['errors']) {
                        case 'request_closed':
                            PopupError('Заявка уже закрыта, добавление невозможно.');
                            break;
                        case 'unrecognized_image_format':
                            PopupError('Загрузка не удалась, неверный формат файла.');
                            break;
                        case 'invalid_file_size':
                            PopupError('Отослать не удалось, размер файла слишком большой.');
                            break;
                        case 'invalid_image_size':
                            PopupError('Отослать не удалось, размер файла слишком большой.');
                            break;
                        default :
                            //TODO: create all possible cases with their error-alerting popups...
                            console.log(data['errors']);
                    }
                    return;
                } else {
                    $('#attachModal').modal('hide');
                    attachmentSentSeen = false;

                    message = data['description'].replace(/(\r\n|\n|\r)/gm, "<br>");
                    time = getFormattedDateTime();

                    if(CM.consultationWindow.css('display') == 'block') {
                        appendingHTML = CM.appendAttachment('me', $('#myId').val(), message, time, 'unread', data['attachment']);
                        CM.addHTMLToChat(appendingHTML);
                        attachmentSentSeen = true;
                    }
                    if (!attachmentSentSeen) {
                        console.log('NOTIFICATION: Your attachment was delivered to doctor.');
                        //TODO add notification to notifications block.
                    }
                }
                clearAttachmentForm();
            },
            error: function(er){
                clearAttachmentForm();
                $('.attach-file').removeClass('inactive').find('.text').text('Добавить файл');
            },

            cache: false,
            contentType: false,
            processData: false
        });
    }).find('.attachmentFile').change(function(){
        var fileFakePath = $(this).val().split('\\');
        var fileName = fileFakePath[fileFakePath.length-1];
        if (fileName.length > 15) {
            var shortName = fileName[0] + fileName[1] + fileName[2] + '...';
            for (var i=fileName.length-11; i<fileName.length; i++) {
                shortName += fileName[i];
            }
            fileName = shortName;
        }
        $('#selectAttachment').val(fileName);
        $('#attachmentForm .request_id').val(CM.request.currentRequest.requestId);
        if ($(this).val() == '') return;

        $("#attachmentForm").submit();
    });

    $('.attach-file').click(function(e){
        e.preventDefault();
        if ($(this).hasClass('sending') || !CM.request.currentRequest.requestOpened || CM.sendingMessage) return;
        $(this).find('.text').text('Выберите файл');
        $('.attachmentFile').trigger('click');
    });

    $('#cancelReserved').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: '/mobile/user/cancel_reserve/?reserve_id='+$('#cancelReserved').data("reserveId"),
            success: function (data) {
                if (data.status != 'OK') {
                    console.log(data.errors);
                } else {
                    $('#reserved-block').remove();
                }
            }
        })
    });

    $('#condirmCodePopup').on('change keyup paste click', function(){
        if ($(this).val() != '') {
            $('#confirmCodePopupSubmit').removeClass('inactive');
        } else {
            $('#confirmCodePopupSubmit').addClass('inactive');
        }
    });

    $('#confirmCodePopupSubmit').click(function(){
        $('#confirmMobilePopupForm').submit();
    });

    $('#rateRequest .rating-star').hover(function(){
        var hovered = $(this);
        $('.rating-star').each(function(){
            if ($(this).data('value') <= hovered.data('value')) {
                $(this).addClass('hovered');
            }
        })
    }, function(){
        $('.rating-star').removeClass('hovered');
    }).click(function(){
        $('#ratingValue').val($(this).data('value'));
        var clicked = $(this);
        $('.rating-star').each(function(){
            if ($(this).data('value') <= clicked.data('value')) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        })
    });

    $('#confirmMobilePopupForm').submit(function(e){
        e.preventDefault();
        var codeSubmit = $('#confirmCodePopupSubmit');
        if (codeSubmit.hasClass('sending')) {
            return;
        }

        codeSubmit.addClass('sending');
        var sendingData = $(this).serialize();
        $.ajax({
            url: "/mobile/user/verification/",
            type: "GET",
            data: sendingData,
            success: function(data) {
                codeSubmit.removeClass('sending');
                if (data['status'] != 'OK') {
                    switch (data['errors']) {
                        case 'wrong_code':
                            $('#codeContainerPopup').addClass('error');
                            break;
                        case 'no_such_user_or_user_already_activated':
                            PopupError('Пользователь уже активирован.');
                            break;
                        default :
                            //TODO: create all possible cases with their error-alerting popups...
                            console.log(data['errors']);
                    }
                } else {
                    SendGAMobileConfirm();
                    $('#activateProfileFormModal').modal('hide');
                    PopupSuccess('Номер успешно подтвержден.');
                    $('#codeContainerPopup').val('').trigger('change');
                    window.location.hash = '';
                }
            },
            error: function(){
                codeSubmit.removeClass('sending');
                console.log('Request error');
                PopupError('Ошибка соединения с сервером');
            }
        });
    });

    $('#resendCodePopupSubmit').click(function(){
        ResendPopupConfirmationCode();
    });

    $('#closeCompabilityBar').click(function(e){
        $('#compatibilityBar').removeClass('visible');
        SetCookie('showCompatibilityBar', 'false', 1);
    });

    WebSocketLocal.CommandResultReceived = function(result) {
        if (result['result']['status'] != 'OK') {
            switch (result['result']['errors']) {
                case 'some_error':
                    break;
                default :
                    //TODO: create all possible cases with their error-alerting popups...
                    console.log(result['result']['errors']);
            }
            return;
        }

        switch (result['name']) {
            case '/mobile/user/online_status/':
                break;
            default:
                console.log(result['result']);
        }
    };

    WebSocketLocal.EventReceived = function(event) {
        console.log(event);
        switch (event['name']) {
            case '/mobile/user/online_status/update/':
                break;
            case '/mobile/system_notification/':
                CM.wsReceived.systemNotification(event);
                break;
            case '/mobile/conversation/send_notification/':
                CM.wsReceived.sendNotification(event);
                break;
            case '/mobile/conversation/take/':
                if (event.arguments.sip) {
                    CM.wsReceived.startCallSip(event);
                } else {
                    CM.wsReceived.takeCall(event);
                }
                break;
            case '/mobile/conversation/start/':
                CM.wsReceived.startCall(event);
                break;
            case '/mobile/conversation/send_message/':
                CM.wsReceived.sendMessage(event);
                break;
            case '/mobile/user/mark_as_read/' :
                CM.wsReceived.markAsRead(event);
                break;
            case '/mobile/doctor/add_conclusion/':
                CM.wsReceived.addConclusion(event);
                break;
            case '/mobile/conversation/drop/':
                CM.wsReceived.dropCall(event);
                break;
            case '/mobile/doctor/5_minutes_left/':
                CM.wsReceived.fiveMinutesLeft(event);
                break;
            case '/mobile/doctor/cancel_request/':
                CM.wsReceived.cancelRequest(event);
                break;
            case '/mobile/user/start_typing/':
                CM.wsReceived.startTyping(event);
                break;
            case '/mobile/user/stop_typing/':
                CM.wsReceived.stopTyping(event);
                break;
        }
    };

    CM.consultationWindow.click(function(){
        if (CM.request.currentRequest.unread > 0) {
            CM.markRequestAsRead(CM.request.currentRequest.requestId);
        }
    }).on('hidden.bs.modal', function () {
        onDropClick();
        CM.emptyCallWindow();
    });

    $(".take").click(function() {
        incomingCallWrapper = $('.incoming-call-wrapper');
        if (incomingCallWrapper.hasClass('sending-action')) return;

        incomingCallWrapper.addClass('sending-action');
        incomingCallWrapper.find('.call-title .text').text('Звонок принят');
        CM.takeIncomingCall($(this).data('video'));
    });

    $('.drop').click(function(e){
        incomingCallWrapper = $('.incoming-call-wrapper');
        if (incomingCallWrapper.hasClass('sending-action')) return;

        incomingCallWrapper.addClass('sending-action');
        incomingCallWrapper.find('.call-title .text').text('Звонок отклонен');
        onDropClick();
    });

    $('.send-button').click(function(){
        if (!$(this).hasClass('disabled')) {
            $(this).parent().submit();
        }
    });

    $('.messaging-form').submit(function(e){
        e.preventDefault();
     //   if (CM.request.currentRequest.callActive) {
            CM.addMessage($(this).find('.input-msg').val());
     //   }
        $(this).find('.input-msg').val('');
    });

    $('#selectAttachment').click(function(e){
        $(this).parent().parent().find('.attachmentFile').trigger('click');
    });

    $("#closeWithDrop").click(function() {
        onDropClick();
    });

    $(".cancelcall").click(function() {
        onDropClick();
    });

    $('.share-video').click(function(e){
        if ($(this).hasClass('not-active')) return;
        toggleVideo();
    });

    $('.share-audio').click(function(e){
        if ($(this).hasClass('not-active')) return;
        toggleAudio();
    });

    $('.call-button').click(function(e){
        if (CM.request.currentRequest.callActive === true) {
            onDropClick();
        }
    });

    CM.consultationWindow.find('.options-button').click(function(e){
        e.preventDefault();
        if ($(this).hasClass('disabled')) return;
        ToggleOptionsState();
    });

    CM.consultationWindow.find('.options-menu-wrapper li').click(function(e){
        e.preventDefault();
        var step = $(this).data('step');
        CM.consultationWindow.find('.options-menu-wrapper li').each(function(){
            if ($(this).data('step') != step) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        });
        CM.consultationWindow.find('.testing-steps li').each(function(){
            if ($(this).data('step') != step) {
                $(this).removeClass('active').removeClass('testing');
                if ($(this).data('step') == 'camera') {
                    testManager.clearCamera();
                } else if ($(this).data('step') == 'microphone') {
                    testManager.clearMicrophone();
                }
            } else {
                $(this).addClass('active');
            }
        });

    });

    $('#feedbackMessageModal').on('shown.bs.modal', function(){
        $('#feedbackRequestId').val('Отзыв по консультации N' + CM.request.currentRequest.requestId);
    }).on('hidden.bs.modal', function(){
        $('#feedbackRequestId').val('');
        $('#feedbackMessage').val('');
    });

    $('#feedbackMessageForm').submit(function(e) {
        e.preventDefault();
        var submitButton = $('#submitFeedback');
        submitButton.addClass('sending');
        $.ajax({
            method:"POST",
            url: "/mobile/enquiry/",
            data: $("#feedbackMessageForm").serialize(),
            dataType: 'json',
            async: false,
            success: function(data) {
                submitButton.removeClass('sending');
                if (data['status'] != 'OK') {
                    switch (data['errors']) {
                        case 'no_text':
                            PopupError('Вы не написали сообщение.');
                            break;
                        default :
                            //TODO: create all possible cases with their error-alerting popups...
                            console.log(data['errors']);
                    }
                } else {
                    $('#feedbackMessageModal').modal('hide');
                    PopupSuccess('Отзыв отправлен в службу поддержки.');
                }
            },
            error: function() {
                submitButton.removeClass('sending');
                PopupError('Не удалось отправить сообщение, попробуйте снова.');
            }
        });
    });

    $('#refillBalancePresets li').click(function(){
        if ($(this).data('value') != '') {
            $('#inputRefillBalance').val($(this).data('value'));
        }
    })

});

function SendCallResolution(val) {
    var submitButton = $('#rateRequest .approve');
    if (submitButton.hasClass('sending')) return;
    submitButton.addClass('sending');
    var ratingValue = $('#ratingValue').val();
    if (typeof ratingValue === 'undefined' ||  ratingValue == 0 || !(CM.request.currentRequest.requestId > 0)) {
        submitButton.removeClass('sending');
        PopupError('Пожалуйста оцените консультацию');
        return;
    }
    $.ajax({
        url: '/mobile/user/send_call_resolution/',
        type: 'GET',
        data: {request_id: CM.request.currentRequest.requestId, resolution: ratingValue, comment: $('#ratingComment').val()},
        success: function(data) {
            submitButton.removeClass('sending');
            EmptyRatingPopup();
            $('#rateRequest').modal('hide');
            if (data.status != 'OK') {
                PopupError('Неизвестная ошибка');
                console.log(data.errors);
            } else {
                PopupSuccess('Ваш ответ принят');
                $('.not-rated').each(function(){
                    if ($(this).data('id') == CM.request.currentRequest.requestId) {
                        $(this).text('');
                    }
                });
                switch (data.resolution) {
                    case 5:
                        break;
                    case 1:
                        break;
                    default:

                }
            }
        },
        error: function(){
            submitButton.removeClass('sending');
            EmptyRatingPopup();
            PopupError('Регистрация не удалась, попробуйте снова.');
            console.log('Request error');
        }
    })
}

function EmptyRatingPopup() {
    ratigPopup = $('#rateRequest');
    ratigPopup.find('.rating-star').removeClass('active').removeClass('hovered');
    ratigPopup.find('.rate-request-id').val('');
    ratigPopup.find('.review').val('');
    ratigPopup.find('.rate-request-doctor-image').attr('src', '/images/avatars/doctor/male/userpic100.jpg');
    ratigPopup.find('.rate-request-doctor').text('Врач');
}