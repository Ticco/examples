var UserWSReceived = function() {

    this.sendNotification = function(event) {
        if (event.result.type == "service_missed_call") {
            if (event.arguments.request_id == CM.request.currentRequest.requestId) {
                if ($('#consultationWindow').css('display') == 'block') {
                    timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
                    appendingHTML = CM.appendCallMissed(getFormattedDateTime(new Date(timestamp)));
                    CM.addHTMLToChat(appendingHTML);
                }
            }
        }
    };
    this.takeCall = function(event) {
        if(CM.guid != event.arguments.guid){
            CM.localDrop();
            CM.emptyCallWindow();
            CM.consultationWindow.modal('hide')
        }
    };
    this.startCall = function(event) {
        CM.goingToHistory = false;
        $(document.body).append('' +
                '<audio class="ringtone" autoplay loop>'+
                    '<source src="/images/ringtone.mp3" type="audio/mpeg">'+
                    '<source src="/images/ringtone.ogg" type="audio/ogg">'+
                '</audio>');
        allowLeave = true;
        CM.openTok.openTokApiKey = openTokApiKey;
        CM.emptyCallWindow();
        CM.setRequestAsActive(event['result']);

        if (CM.consultationWindow.hasClass('fullscreen')) {
            CM.consultationWindow.find('.action-buttons').find('.messages').addClass('new');
        }

        $('input[name=request_id]').val(CM.request.currentRequest.requestId);
        $('#videos').css('background-image', 'url("/mobile/user/userpic/'+CM.request.currentRequest.doctorId+'/?' + new Date().getTime()+'")');
        CM.showCallPopup();
    };
    this.dropCall = function(event){
        if (event.result.conversation_id == CM.request.currentRequest.conversationId ||
            event.result.request_id == CM.request.currentRequest.requestId) {

            CM.request.currentRequest.unread++;
            IncrementUnreadHistory(event['result']['request_id']);

            CM.localDrop();
            CM.hideCallPopup();
            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
            status = (event.event_originator_id == CM.myId ? '' : 'unread');
            appendingHTML = '';
            switch (event.result.status) {
                case 'dropped':
                    appendingHTML = CM.appendCallDropped(getFormattedDateTime(new Date(timestamp)), status);
                    break;
                case 'missed':
                    appendingHTML = CM.appendCallMissed(getFormattedDateTime(new Date(timestamp)), status);
                    break;
                case 'finished':
                    appendingHTML = CM.appendCallFinished(getFormattedDateTime(new Date(timestamp)), status);
                    break;
                default:
                    appendingHTML = CM.appendCallDroppedSystem(getFormattedDateTime(new Date(timestamp)), status);
            }

            isUnread = false;

            if(CM.consultationWindow.css('display') == 'block') {
                CM.addHTMLToChat(appendingHTML);
                isUnread = true;
            }
        }
    };
    this.startCallSip = function(event) {
        IncrementUnreadHistory(event['result']['request_id']);
        if (event.arguments.request_id == CM.request.currentRequest.requestId) {
            appendingHTML = CM.appendCallStarted(getFormattedDateTime(new Date(timestamp)), 'SIP', 'unread');
            CM.addHTMLToChat(appendingHTML);
            CM.request.currentRequest.unread++;
        }
    };
    this.addConclusion = function(event) {
        if (event.arguments.request_id == CM.request.currentRequest.requestId) {
            var conclusionSeen = false;
            var message = event['result']['conclusion'].replace(/(\r\n|\n|\r)/gm, "<br>");
            appendingHTML = '';
            timestamp = (typeof event['result']['unix_timestamp'] == 'undefined' ? new Date() : parseInt(event['result']['unix_timestamp'])*1000);
            appendingHTML = CM.appendConclusion('opponent', event['event_originator_id'], message, getFormattedDateTime(new Date(timestamp)), 'unread');

            if(CM.consultationWindow.css('display') == 'block') {
                CM.addHTMLToChat(appendingHTML);
                conclusionSeen = true;
            }
            if (!conclusionSeen) {
                CM.request.currentRequest.unread++;
                IncrementUnreadHistory(event['arguments']['request_id']);
                //TODO add notification to notifications block.
            }
        }
    };
    this.fiveMinutesLeft = function(event) {
        if (CM.callUI.consultationWindow &&
            (
                (event.arguments.request_id && CM.request.currentRequest.requestId != event.arguments.request_id) || (event.request.request_id && CM.request.currentRequest.requestId != event.request.request_id)
            )
        ) {
            if(event.result.next_slot) {
                PopupSuccess('Время вашей консультации подходит к концу. У врача назначена консультация, которая начнется через 5 минут.', 'Внимание!');
            }
        }
    };
    this.cancelRequest = function(event) {
        if(event['arguments']['request_id'] == CM.request.currentRequest.requestId) {
            if (event['result']['payment_type'] == 'promo') {
                appendingLogHTML = CM.appendPromoRefund(event['timestamp']);
            } else {
                appendingLogHTML = CM.appendMoneyRefund(event['timestamp']);
            }
            CM.addHTMLToChat(appendingHTML);
            if (event['arguments']['user_type'] == 'user') {
                appendingLogHTML = CM.appendRequestCanceledUser(event['timestamp']);
            } else {
                appendingLogHTML = CM.appendRequestCanceledDoctor(event['timestamp']);
            }
            CM.addHTMLToChat(appendingHTML);
        }
        if (event['arguments']['user_type'] == 'user') {
            if (event['arguments']['payment_type'] == 'promo') {
                PopupSuccess('Консультация удачно отменена, промокод выслан на эл. почту.');
            } else {
                PopupSuccess('Консультация удачно отменена, средства возвращены на счет.');
            }
        } else {
            if (event['arguments']['payment_type'] == 'promo') {
                PopupError('Консультация была отменена врачом, промокод выслан на эл. почту.');
            } else {
                PopupError('Консультация была отменена врачом, средства возвращены на счет.');
            }
            info = event['result']['info'];
            doctor = {
                id:             info.id,
                first_name:     info.doctor_first_name,
                middle_name:    (typeof info.doctor_middle_name !== 'undefined' ? info.doctor_middle_name : ''),
                last_name:      info.doctor_last_name
            };
            ShowRatingPopup(event['arguments']['request_id'], doctor)
        }
    };
};

var UserCallModel = function() {

    this.guid = '';

    this.goingToHistory = false;

    this.wsReceived = new UserWSReceived();

    this.showCallPopup = function() {
        CM.consultationWindow.modal('show').find('.modal-body').removeClass('inCall').removeClass('options-shown').removeClass('outgoingCall').addClass('incomingCall').end()
            .find('#consultationWindowHeader')
                .find('.avatar').attr('src', '/mobile/user/userpic/' + this.request.currentRequest.doctorId + '/thumb/?' + new Date().getTime()+'').end()
                .find('.name').text(this.request.currentRequest.doctorName).end()
                .find('.profession').text(this.request.currentRequest.doctorProfession).end()
                .end();
       //     .find('.options-button').addClass('disabled').attr('title', 'Тестировать можно только вне активного звонка.').end();
        this.callUI.callPopupVisible = true;
        this.callState = 'calling';
        this.getChat();
    };

    this.prepareForChat = function() {
        var callWindow = $('#consultationWindow');
        var callWindowHeader = $('#consultationWindowHeader');

        callWindow.find('.modal-body').removeClass('incomingCall');
        callWindow.find('.video-wrapper').removeClass('visible');
        callWindow.find('.chat-wrapper').addClass('wide');
    };

    this.redirectToHistory = function(requestId){
        if (requestId != '') {
            document.location.href = '/history/?request_id='+requestId;
        } else {
            document.location.href = '/history/';
        }
    };

    this.localDrop = function() {
        this.guid = null;
        window.onbeforeunload=null;
        this.openTok.disconnectStuff();
        this.openTok.animateClosingAllowStreamNotification();

        this.exitFullscreen();

        CM.consultationWindow.find('.modal-body').removeClass('incomingCall').removeClass('inCall')
            .find('.call-button').removeClass('drop-call').end()
            .find('.video-wrapper').removeClass('visible').end()
            .find('.chat-wrapper').addClass('wide').end()
            .find('.share-video').addClass('not-active').addClass('disabled').end()
            .find('.share-audio').addClass('not-active').addClass('disabled').end();

        $('.beep').remove();
        $('.ringtone').remove();
   //     $('#consultationWindow').modal("hide");
        this.request.currentRequest.callActive = false;
        this.callUI.callPopupVisible = false;
        this.callUI.consultationWindow = false;
        this.callUI.callCirclesIterator = 0;
        this.callState = 'inactive';
        updateScrollbar($('#chat-container'));
    };

    this.addMessage = function(message)  {

        if (this.sendingMessage) return;

        var callWindow = $('#consultationWindow');

        updateScrollbar($('.messaging-wrapper'));
        callWindow.find('.send-message-wrapper .input-msg').val('');

        if ($.trim(message) != '') {
            CM.sendingMessage = true;
            CM.tempMessage = message;
            CM.consultationWindow.find('.messaging-form').attr('title', 'Вы сможете отправить следующее сообщение, когда предыдущее будет обработано.');

            appendingHTML = this.appendMessage('me', $('#myId').val(), 'Сообщение отсылается', '', 'unread');
            $('.chat-messages').append(appendingHTML);
            updateScrollbar($('.messaging-wrapper'));
            if (this.request.currentRequest.requestId === null || typeof this.request.currentRequest.requestId =='undefined') {
                PopupError('Нет активного собеседника, невозможно отослать сообщение');
            } else {
                var _this = this;
                var arguments = {};
                arguments['message'] = message;
                arguments['request_id'] = this.request.currentRequest.requestId;
                $.ajax({
                    url: '/mobile/conversation/send_message/',
                    data: arguments,
                    success: function(result) {
                        if (result['status'] != 'OK') {
                            CM.sendingMessage = false;
                            CM.consultationWindow.find('.messaging-form').removeAttr('title').end()
                                .find('.tempMessage').remove().end()
                                .find('.input-msg').removeAttr('style').val(CM.tempMessage).trigger('keypress').end();
                            CM.tempMessage = '';

                            switch (result['errors']) {
                                case 'request_closed':
                                    PopupError('Врач завершил консультацию․ Ваше сообщение не может быть отправлено.');
                                    break;
                                case 'missing_arguments':
                                    PopupError('Произошла ошибка в процессе отправки сообщения, попробуйте перезагрузить страницу и отправить сообщение снова.');
                                    break;
                                default :
                                    //TODO: create all possible cases with their error-alerting popups...
                                    console.log(result['errors']);
                            }
                        } else {
                            CM.sendingMessage = false;
                            CM.consultationWindow.find('.messaging-form').removeAttr('title').end()
                                .find('.tempMessage').remove().end()
                                .find('.input-msg').removeAttr('style').val('').trigger('keypress').end();
                            CM.tempMessage = '';

                            _this.showSentMessage(result);
                        }
                    },
                    error: function() {
                        CM.sendingMessage = false;
                        CM.consultationWindow.find('.messaging-form').removeAttr('title').end()
                            .find('.tempMessage').remove().end()
                            .find('.input-msg').removeAttr('style').val(CM.tempMessage).trigger('keypress').end();
                        CM.tempMessage = '';

                        PopupError('Ошибка связи с сервером, сообщение не отослано.')
                    }
                });
                //  WebSocketLocal.RequestCommand('/mobile/conversation/send_message/', arguments);
            }
        }
        return false;
    };

    this.hideCallPopup = function() {

        CM.consultationWindow.find('.modal-body').removeClass('incomingCall');
        $('.incoming-call-wrapper').removeClass('sending-action');

        //   .find('.options-button').removeClass('disabled').removeAttr('title').end();

        this.callUI.callPopupVisible = false;
        this.callState = 'inactive';
    };

    this.onDropClick = function() {
        $('.ringtone').remove();
        if (this.callUI.consultationWindow||this.callUI.callPopupVisible) {
            if (this.request.currentRequest.conversationId !== null) {
                if(CM.guid){
                    this.sendDrop();
                }
                this.localDrop();
                this.hideCallPopup();
            }
        }
        return false;
    };

    this.setRequestAsActive = function(request) {
        obj = new Request();
        obj.userId = request['user_id'];
        obj.requestOpened = request['opened'];
        obj.userName = request['user_first_name'];
        obj.requestId = request['request_id'];
        obj.requestType = 'video';
     //   obj.requestType = request['request_type'];
        obj.doctorId = request['doctor_id'];
        obj.doctorName = request['last_name']+' '+request['first_name']+' '+request['middle_name'];
        if (typeof request['doctor_profession'] !== 'undefined') {
            obj.doctorProfession = request['doctor_profession'];
        } else {
            obj.doctorProfession = 'Врач-педиатр';
        }
        obj.conversationId = request['conversation_id'];
        obj.callActive = true;

        this.request.currentRequest = obj;
        this.openTok.requestType = request['request_type'];
    };

    this.addLogToChat = function(log) {
        this.addToChat(log, this.request.currentRequest.doctorId, this.request.currentRequest.doctorName)
    };

    this.appendInitHTML = function(time) {

        greeting = (this.request.currentRequest.requestType == 'chosen'  ? 'За 24 часа до начала консультации можно <a href="#" id="cancelRequest" onclick="CancelActiveRequest();">отменить</a> или <a href="#" id="postponeRequest" onclick="PostponeActiveRequest();">перенести</a> консультацию, в случае отмены средства будут возвращены на Ваш счет.' : 'Дежурный педиатр свяжется с Вами в течение нескольких минут. Сейчас Вы можете отправить доктору сообщение или изображение.');

        chosenActionsHTML = '';

        if (this.request.currentRequest.requestType == 'chosen') {
            chosenActionsHTML += '' +
                '<div class="message system req-serving">' +
                    '<div class="avatar-cont">' +
                        '<img src="/images/logo-face.png">' +
                    '</div>' +
                    '<div class="textblock">' +
                        '<div class="text">' +
                            '<span class="title"><a href="/doctors/?id='+this.request.currentRequest.doctorId+'">'+this.request.currentRequest.doctorName+'</a> свяжется с Вами в назначенное время.</span>' +
                            '<span class="text">Сейчас Вы можете отправить доктору сообщение или изображение.</span>' +
                        '</div>' +
                        '<span class="datetime"> - ' + getLiteralDate(time) + '</span>' +
                    '</div>' +
                '</div>' +
                '<div class="clearFloat"></div>' +
            '';
        }

        htmlText = '' +
            '<div class="system-notification req-created {$status}">' +
                '<div class="avatar-cont"></div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Начало консультации</span>' +
                        '<span class="datetime"> - ' + getLiteralDate(time) + '</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
            '' +
            '<div class="message system app-initiated {$status}">' +
                '<div class="avatar-cont">' +
                    '<img src="/images/logo-face.png">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="text">' +
                        'Уважаемый пользователь!' +
                        '<br>' +
                        'Спасибо! Ваш запрос на консультацию отправлен.' +
                        '<br>' +
                        '<br>' +
                        greeting +
                    '</div>' +
                    '<span class="datetime"> - ' + getLiteralDate(time) + '</span>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
            chosenActionsHTML +
        '';
        return htmlText;
    };

    this.showResolutionPopup = function(){
        $('#rateRequest .rate-request-doctor').text(NameShortener(this.request.currentRequest.doctorName));
        $('.rate-request-doctor-image').attr('src', '/mobile/user/userpic/'+this.request.currentRequest.doctorId+'/thumb/');
        $('#rateRequest').modal('show');
    };

    this.appendLogMessage = function(author_class, author_id, message, time, status, object_id) {
        messageClass = '';
        if (time == '') messageClass = ' tempMessage';
        if ($.trim(message) == '') message = '&nbsp;';
        messageClass += (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="message '+author_class+messageClass+'">' +
                '<div class="avatar-cont">' +
                    '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="text">' +
                        message +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';

        return htmlText;
    };

    this.appendLogAttachment = function(author_class, author_id, message, time, status, object_id) {
        messageClass = '';
        if (time == '') messageClass = ' tempMessage';
        if ($.trim(message) == '') message = '&nbsp;';
        messageClass += (status == 'unread' ? ' unread' : '');
        switch (message) {
            case 'bmp':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/bmp-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'tiff':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/tiff-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'pdf':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/pdf-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'doc':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/doc-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'docx':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/doc-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'xls':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/xls-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            case 'xlsx':
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock">' +
                            '<div class="text-container">' +
                                '<a href="/mobile/user/get_request_attachment/?id='+object_id+'" target="_blank"><img src="/images/xls-icon.png" class="file-link"></a>' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
                break;
            default:
                htmlText = '' +
                    '<div class="message ' + author_class + messageClass + ' attachment">' +
                        '<div class="avatar-cont">' +
                            '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                        '</div>' +
                        '<div class="textblock image-attachment" style="background-image: url(\'/mobile/user/get_request_attachment/?id='+object_id+'\');" onclick="ShowAttachment('+object_id+')">' +
                            '<div class="text-container">' +
                                '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="clearFloat"></div>' +
                    '</div>' +
                    '<div class="clearFloat"></div>' +
                '';
        }

        return htmlText;
    };

    this.appendLogConclusion = function(author_class, author_id, message, time, status, doctor) {
        messageClass = (status == 'unread' ? ' unread' : '');
        htmlText = '' +
            '<div class="system-notification conclusion ' + messageClass + '">' +
                '<div class="avatar-cont">' +
                    '' +
                '</div>' +
                '<div class="textblock">' +
                    '<i></i>' +
                    '<div class="cont">' +
                        '<span class="text">Доктор добавил заключение</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';
        htmlText += '' +
            '<div class="system-notification opponent conclusion-detailed {$status}">' +
                '<div class="avatar-cont">' +
                    '<img src="/mobile/user/userpic/'+author_id+'/thumb/">' +
                '</div>' +
                '<div class="textblock">' +
                    '<div class="cont">' +
                        '<span class="text">Заключение врача</span>' +
                        '<span class="doctor">' + doctor + '</span>' +
                        '<span class="datetime"> - '+getLiteralDate(time)+'</span>' +
                        '<span class="conclusion-text">' + message + '</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="clearFloat"></div>' +
        '';

        return htmlText;
    };

    this.openChat = function(element) {
        var request_id = $(element).data("requestId");
        var doctor_id = $(element).data('requestOriginator');
        CM.request.currentRequest = new Request();
        CM.request.currentRequest.requestId = request_id;
        CM.request.currentRequest.doctorId = doctor_id;

        $('input[name=request_id]').val(this.request.currentRequest.requestId);
        this.callUI.consultationWindow = true;
        this.request.currentRequest.callActive = true;


      //  this.customizeCallWindow('text');

        this.getChat();

        $('#consultationWindow').modal("show");
        this.callUI.consultationWindow = true;

    };

    this.openChatFromHistory = function() {
        var callWindow = $('#consultationWindow');
        var callWindowHeader = $('#consultationWindowHeader');

        $('input[name=request_id]').val(this.request.currentRequest.requestId);

        callWindowHeader.find('.avatar').attr('src', '/mobile/user/userpic/' + this.request.currentRequest.doctorId + '/thumb/?' + new Date().getTime()+'');
        callWindowHeader.find('.name').text(this.request.currentRequest.doctorName);
        callWindowHeader.find('.profession').text(this.request.currentRequest.doctorProfession);

        callWindow.modal('show');

        this.getChat();

        this.callUI.consultationWindow = true;

    };

    this.takeIncomingCall = function(videoState){
        if (!this.request.currentRequest.callActive) {
            PopupError('Нет входящего звонка.');
        } else {
            if (this.callState == 'calling') {
                var _this = this;
                var arguments = {};
                if (videoState == 2) {
                    arguments['conversation_id'] = this.request.currentRequest.conversationId;
                    $.ajax({
                        url: '/mobile/conversation/answer_as_chat/',
                        data: arguments,
                        success: function(result) {
                            _this.prepareForChat();
                        }
                    });
                } else {
                    this.guid = Math.floor(Math.random() * 90000000)+10000000;
                    arguments['conversation_id'] = this.request.currentRequest.conversationId;
                    arguments['opentok_version'] = version;
                    arguments['guid'] = this.guid;

                    var streamVideo = false;
                    if (videoState == '1') {
                        streamVideo = true;
                    }

                    $.ajax({
                        url: '/mobile/conversation/take/',
                        data: arguments,
                        success: function(result) {
                            window.onbeforeunload = CM.unload;
                            $('.ringtone').remove();
                            _this.takeCall(result, streamVideo);
                        },
                        error: function(e) {
                            $('.incoming-call-wrapper').removeClass('sending-action');
                        }
                    });
                }
            }
        }
        return false;
    };

    this.takeCall = function(result, streamVideo) {
        this.hideCallPopup();
        if (typeof streamVideo === null) {
            streamVideo = false;
        }

        this.openTok.streamVideo = streamVideo;

        this.callUI.consultationWindow = true;
        this.request.currentRequest.callStartTime = new Date().getTime();

        timestamp = (typeof result['unix_timestamp'] == 'undefined' ? new Date() : parseInt(result['unix_timestamp'])*1000);
        appendingHTML = CM.appendCallStarted(getFormattedDateTime(new Date(timestamp)));
        CM.addHTMLToChat(appendingHTML);

        //$('#consultationWindow').modal("show");
        this.createConnection(result);
    };

    this.dropCall = function(result) {
        this.hideCallPopup();
        this.localDrop();

     //   $('#consultationWindow').modal("hide");
     //   this.callUI.consultationWindow = false;
    };

    this.createConnection1 = function(object) {
        if (typeof this.openTok.streamVideo === 'undefined' || this.openTok.streamVideo === null) {
            this.openTok.streamVideo = false;
        }
        CM.consultationWindow.find('.modal-body').addClass('inCall')
            .find('.chat-wrapper').removeClass('wide').end()
            .find('.send-call').addClass('drop-call');

        updateScrollbar($('#chat-container'));

        this.openTok.token = object['token'];
        this.openTok.sessionId = object['session'];

        this.openTok.publishVideo = this.openTok.streamVideo;
        if (this.openTok.streamVideo) {
            //this.showVideoWrapper();
            callWindow.find('.share-video').removeClass('disabled');
        } else {
            //this.hideVideoWrapper();
            callWindow.find('.share-video').addClass('disabled');
        }
        callWindow.find('.video-wrapper').addClass('visible');

        this.openTok.publishAudio = true;
        callWindow.find('.share-audio').removeClass('disabled');


        switch (this.request.currentRequest.requestType) {
            case 'audio':
                this.openTok.pubOptions = {publishAudio: true, publishVideo: false, width: 120, height: 95};
                this.openTok.initOpentok();
                this.talkingTimer();
                if( typeof this.updateConversation == 'function') {
                    this.updateConversation();
                }
                break;
            case 'text':
                break;
            default :
                this.openTok.setPubOptions({publishAudio: true, publishVideo: this.openTok.streamVideo, width: 120, height: 95});
                this.openTok.initOpentok();
                this.talkingTimer();
                if( typeof this.updateConversation == 'function')
                {
                    this.updateConversation();
                }
                break;
        }
    };

    this.showVideoWrapper = function() {
        CM.consultationWindow.find('.share-video').removeClass('disabled').end()
            .find('.video-wrapper').addClass('visible').end()
            .find('.chat-wrapper').removeClass('wide').end();

        updateScrollbar($('#chat-container'));

    };

    this.hideVideoWrapper = function() {
        var callWindow = $('#consultationWindow');
        callWindow.find('.share-video').addClass('disabled');
        callWindow.find('.video-wrapper').removeClass('visible');
        callWindow.find('.chat-wrapper').addClass('wide');
    };

};

UserCallModel.prototype = new CallModel();
UserWSReceived.prototype = new WSReceived();

