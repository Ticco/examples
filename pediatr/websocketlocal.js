var WebSocketLocal =
{
    Socket: null,
    CommandsHistory: {},
    SessionId: null,
    Host: null,

    Connecting: function () {
    },
    Connected: function () {
    },
    Disconnecting: function () {
    },
    Disconnected: function () {
        console.log('Disconnected')
        window.setTimeout("WebSocketLocal.RequestCommand('/mobile/user/online_status/', null, null);",30000);
    },
    Error: function (error) {
    },
    CommandRequestComplete: function (command) {
    },
    CommandResultReceived: function (data) {
    },
    EventReceived: function (event) {
    },

    Connect: function () {
        try {
            this.InitSocket(this);
        }
        catch (ex) {
            return ex;
        }
        return true;
    },

    Disconnect: function () {
        this.Disconnecting();
        try {
            if (this.Socket !== null) {
                this.Socket.close();
                this.Socket = null;
            }
        }
        catch (ex) {
            return ex;
        }
        this.Disconnected();
        return true;
    },

    RequestCommand: function (name, arguments, time_offset) {
        if (typeof(time_offset) === 'undefined') {
            time_offset = 0;
        }
        if (typeof(arguments) === 'undefined') {
            arguments = null;
        }
        if (this.Socket !== null && this.Socket.readyState !== 1) {
            this.Socket.close();
            this.Socket = null;
        }
        if (this.Socket === null) {
            this.InitSocket(this.RequestCommandInternal, name, arguments, time_offset);
            return;
        }
        this.RequestCommandInternal(name, arguments, time_offset);
    },


    InitSocket: function (callback_on_connect, arg_name, arg_arguments, arg_time_offset) {
        try {
            if (this.Socket !== null) {
                var socketStatus = this.Disconnect();
                if (socketStatus !== true) {
                    return socketStatus;
                }
            }
            WebSocketLocal.Connecting();
            this.Socket = new WebSocket(this.Host);

            this.Socket.onopen = function () {
                if (this.readyState === 1) {
                    WebSocketLocal.Connected('Websocket connected - status:' + this.readyState);
                    if (callback_on_connect !== null &&
                        typeof callback_on_connect === 'function') {
                        callback_on_connect(arg_name, arg_arguments, arg_time_offset);
                    }
                } else {
                    WebSocketLocal.Error('Websocket connection error - status:' + this.readyState);
                    WebSocketLocal.Disconnect();
                }
            };
            this.Socket.onmessage = function (msg) {
                try {
                    WebSocketLocal.ParseResponsePacket(msg.data);
                }
                catch (ex) {
                    WebSocketLocal.Error(ex);
                }
            };
            this.Socket.onclose = function () {
                WebSocketLocal.Disconnected();
            };
        }
        catch (ex) {
            this.Error(ex);
        }
    },

    RequestCommandInternal: function (name, arguments, time_offset) {
        function GenerateGuid() {
            function _p8(s) {
                var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
            }

            return _p8() + _p8(true) + _p8(true) + _p8();
        };
        var packet = {};
        packet['type'] = "command";
        packet['session_id'] = WebSocketLocal.SessionId;
        packet['data'] = {};
        packet['data']['name'] = name;

        // store the unique id of the command in the arguments list which will be returned back to the client
        if (arguments === null || typeof(arguments) === 'undefined') {
            arguments = {};
        }
        arguments['client_command_id'] = GenerateGuid();
        packet['data']['arguments'] = arguments;
        packet['data']['time_offset'] = time_offset;

        var jsonCommand = JSON.stringify(packet);
        WebSocketLocal.CommandsHistory[arguments['client_command_id']] = packet;
        WebSocketLocal.Socket.send(jsonCommand);
        //console.log(jsonCommand);
        WebSocketLocal.CommandRequestComplete(packet);
    },

    ParseResponsePacket: function (jsonEvent) {
        var packet = JSON.parse(jsonEvent);
        //console.log(jsonEvent);
        if (packet === null || typeof packet === 'undefined') {
            throw "invalid_packet_recieved";
        }
        if (!('type' in packet)) {
            throw "no_packet_type_recieved";
        }

        if (packet['type'] !== 'response' && packet['type'] !== 'event' && packet['type'] !== 'command_result') {
            throw "invalid_packet_type_recieved; " + packet['type'];
        }

        if (packet['data'] === null || typeof packet['data'] === 'undefined') {
            throw "undefined_packet_data_received";
        }

        if (packet['type'] === 'command_result') {
            delete packet['type'];

            if (packet['data']['result'] === null || typeof packet['data']['result'] === 'undefined') {
                throw "undefined_command_result_received";
            }

            if (packet['data']['result'] === null && typeof packet['data']['result'] === 'undefined') {
                throw "invalid_command_result_received";
            }

            objToRetrun = {};
            objToRetrun = packet['data'];

            //delete this.CommandsHistory[data['client_command_id']];
            WebSocketLocal.CommandResultReceived(objToRetrun);
        } else if (packet['type'] === 'event') {
            delete packet['type'];
            if (packet['data']['result'] !== null && typeof packet['data']['result'] !== 'undefined') {
                //packet['data']['result'] = JSON.parse(packet['data']['result']);
            }
            objToReturn = packet['data'];
            WebSocketLocal.EventReceived(objToReturn);
        }
    }
}
